package com.example.mysqlconnectactivity.check;

import static com.example.mysqlconnectactivity.check.NoteActivity.HAVE_RESET_TOGGLEBUTTON;
import static com.example.mysqlconnectactivity.check.NoteActivity.checkItemNoString;
import static com.example.mysqlconnectactivity.sqlite.SqlDataBaseHelper.DataBaseName;
import static com.example.mysqlconnectactivity.sqlite.SqlDataBaseHelper.DataBaseTable;
import static com.example.mysqlconnectactivity.sqlite.SqlDataBaseHelper.DataBaseVersion;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.DateFormat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mysqlconnectactivity.ListAdapter;
import com.example.mysqlconnectactivity.R;
import com.example.mysqlconnectactivity.databinding.ActivityCheckItemBinding;
import com.example.mysqlconnectactivity.sqlite.SqlDataBaseHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CheckItemActivity extends AppCompatActivity {
    //宣告
    private ActivityCheckItemBinding binding;
    public static TextView tv_SheetNm, tv_Today;
    private Calendar mCal;
    private CharSequence s;
    private int sheetNumber = 1;
    private String sheetNumberString;
    private ListView checkItemListView;
    private ListAdapter myListAdapter;
    private List<String> dayList;
    private static SQLiteDatabase db; //可以利用此類別物件去新增，查詢，修改，刪除資料表資料
    private static SqlDataBaseHelper sqlDataBaseHelper; //自己建立產生資料庫及資料表的類別
    private Bundle bundle;
    private Button submitBT;
    public static ProgressBar uploadProgressBar;
    private long upLoadNo;
    private String SheetNameString = "日常巡檢 - 依每日";
    private String NameID = "Raden"; //檢查人
    public Handler updateViewHandler;
    public static TextView percentageTV;
    public boolean isDouble;
    List<Boolean> firstTBbool, secondTBbool;
    public static String time;
    public static List<ToggleButton> firstTB, secondTB;
    public Context context;
    private boolean isOldData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_check_item);
        //產生實體
        //在build.gradle將 viewBinding 設為 true，則新增一個 view 就會自動產生一個 viewBinding.java的類別
        binding = ActivityCheckItemBinding.inflate(getLayoutInflater());
        tv_SheetNm = binding.tvSheetNumber;
        tv_Today = binding.tvToday;
        checkItemListView = binding.lvItemCheck;
        submitBT = binding.btSubmit;
        uploadProgressBar = binding.progressBar;
        percentageTV = binding.tvpercentage;
        updateViewHandler = new Handler();
        dayList = new ArrayList<>();
        firstTBbool = new ArrayList<>();
        secondTBbool = new ArrayList<>();
        firstTB = new ArrayList<>();
        secondTB = new ArrayList<>();
        bundle = this.getIntent().getExtras(); //接收Intent過來的資料

        //產生布局
        setContentView(binding.getRoot());
        context = this;

        //****設定今天日期*****
        mCal = Calendar.getInstance();
        if (bundle != null) {
            isOldData = bundle.getBoolean("isOld"); //為了檢查是否為當天的歷史資料
            //得到bundle傳回的值，如果不是null則修改今天的日期
            if (bundle.getString("Plan") != null) {
                time = bundle.getString("Plan");
                if (time != null) {
                    time.substring(0, 10);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = null;
                    try {
                        date = sdf.parse(time);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    mCal.setTime(date);
                }
            }
        }
        s = DateFormat.format("yyyy-MM-dd", mCal.getTime());
        tv_Today.setText(s.toString());
        //****END****

        //設定編號
        sheetNumberString = s.toString() + "-" + sheetNumber;
        tv_SheetNm.setText(sheetNumberString);

        //用日期檢查是否不是當日報表或只是查看歷史資料，如果是就無法上傳資料庫
        if ((DateFormat.format("yyyy-MM-dd", mCal.getTime())).toString().compareTo(DateFormat.format("yyyy-MM-dd", Calendar.getInstance().getTime()).toString()) != 0 || isOldData == true) {
            tv_SheetNm.setText(bundle.getString("Plan"));
            submitBT.setVisibility(View.GONE);
            Toast.makeText(getApplicationContext(), "非當日巡檢表，不可上傳資料庫", Toast.LENGTH_LONG).show();
        } else {
            //檢查是否是重複表單，重新設置編號
            isDouble = dbCheckData();
            if (isDouble) {
                submitBT.setText("重新上傳表單");
            }
        }

        //如果是假日就不顯示檢查資料
        if (!(mCal.get(Calendar.DAY_OF_WEEK) == 1 || mCal.get(Calendar.DAY_OF_WEEK) == 7)) {
            //判斷是否是要查歷史資料，再從資料庫查找資料
            if (isOldData == false) {
                dayList = getCheckList();
            } else {
                dayList = getHistoryCheckList();
            }
            // ListView 中所需之資料參數可透過修改 Adapter 的建構子傳入(畫面載入時先產生每日檢查事項)
            myListAdapter = new ListAdapter(CheckItemActivity.this, dayList);
            //設定 ListView 的 Adapter
            checkItemListView.setAdapter(myListAdapter);
        } else {
            submitBT.setEnabled(false);
            submitBT.setText("沒有需要處理的巡檢單");
        }
        //開啟改變ListView顯示畫面(Toggle Button)的執行序
        Thread mythread = new Thread(updateView);
        mythread.start();

        //Toast.makeText(getApplicationContext(), "onCreate", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        //檢查NoteActivity傳回來的資料，改變ToggleButton的狀態
        if (HAVE_RESET_TOGGLEBUTTON) {
            //得到傳進來的static字串
            String[] tempArray = checkItemNoString.split("-");
            //得到當前的ListView的ViewGroup
            ViewGroup v = (ViewGroup) (getWindow().getDecorView().findViewById(R.id.lv_itemCheck));
            ToggleButton tb2 = v.getChildAt(Integer.parseInt(tempArray[4])).findViewById(R.id.toggleButton2);
            tb2.setChecked(false);
            HAVE_RESET_TOGGLEBUTTON = false;
        }
        //Toast.makeText(getApplicationContext(), "onResume", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStop() {
        super.onStop();
        //Toast.makeText(getApplicationContext(), "onStop", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //當Activity完全退出時強制寫入資料表
        boolean haveRec = haveRecordCheck();
        if (haveRec && submitBT.getVisibility() != View.GONE && isOldData == false) {
            if ((DateFormat.format("yyyy-MM-dd", mCal.getTime())).toString().compareTo(DateFormat.format("yyyy-MM-dd", Calendar.getInstance().getTime()).toString()) != 0) {
                writeDetectResultTable();
                //寫入另一個資料表
                writeDetectInfoTable();
            }
        }
        //Toast.makeText(getApplicationContext(), "onDestroy", Toast.LENGTH_SHORT).show();
    }

    //改變ListView顯示畫面的執行序
    Runnable updateView = new Runnable() {
        @Override
        public void run() {
            //從資料庫拿取資料存入預先定義的陣列中
            getDBdataToChangeView();
            //使用Handler的post方法去改變UI介面(如果沒有歷史清單則不改變狀態)
            if (firstTB.size() != 0 && secondTB.size() != 0) {
                updateViewHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        for (int i = 0; i < firstTB.size(); i++) {
                            firstTB.get(i).setChecked(firstTBbool.get(i));
                            ListAdapter.isRefused = true; //禁止跳轉頁面
                            secondTB.get(i).setChecked(secondTBbool.get(i));
                            //禁止使用者再去改變botton狀態
                            firstTB.get(i).setClickable(false);
                            secondTB.get(i).setClickable(false);
                            //取消焦點
                            firstTB.get(i).setFocusable(false);
                            secondTB.get(i).setFocusable(false);
                            checkItemListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    //得到父Activity的表單編號
                                    String sheetNoString = tv_SheetNm.getText().toString();
                                    //產生紀錄編號
                                    String recordNoString = sheetNoString + "-" + position;
                                    Intent intent = new Intent();
                                    Bundle bundle = new Bundle();
                                    intent.setClass(context, NoteActivity.class);
                                    bundle.putString("RecNo", recordNoString);
                                    bundle.putString("RecName", dayList.get(position));
                                    bundle.putBoolean("IsHistory", true);
                                    intent.putExtras(bundle);
                                    context.startActivity(intent);
                                }
                            });
                        }
                    }
                });
            }
        }
    };

    //從資料庫查找資料
    public List<String> getCheckList() {
        List<String> tempList = new ArrayList<>();
        //建立物件
        DataBaseName = "Inspection_System";
        DataBaseTable = "Item";
        DataBaseVersion = 2;
        sqlDataBaseHelper = new SqlDataBaseHelper(this, DataBaseName, null, DataBaseVersion, DataBaseTable);
        db = sqlDataBaseHelper.getWritableDatabase();//開啟資料庫
        //檢查日期
        String checkTypeString = dayCheck();
        Cursor c = db.rawQuery("SELECT * FROM Item WHERE (ClassNo=1" + checkTypeString + ") and Enabled='1'", null);
        if (c.getCount() > 0) {
            c.moveToFirst(); //移到第一行
            do {        // 逐筆讀出資料
                tempList.add(c.getString(1));
            } while (c.moveToNext());    // 有一下筆就繼續迴圈
        }
        return tempList;
    }

    //檢查今天日期需要做什麼檢查
    public String dayCheck() {
        String tempString = "";
        int w = mCal.get(Calendar.DAY_OF_WEEK) - 1; //得到今天星期幾
        int m = mCal.get(Calendar.DAY_OF_MONTH); //得到一個月的第幾天
        int s = mCal.get(Calendar.MONTH) + 1;//得到第幾月

        if (w == 1) { //星期一
            tempString += " OR ClassNo=2";
            SheetNameString = "週巡檢 - 依每週";
        }
        if (m == 1) { //一個月的第一天
            tempString += " OR ClassNo=3";
            SheetNameString = "月巡檢 - 依每月";
        }
        if ((s == 4 || s == 7 || s == 10) && m == 1) { //每一季
            tempString += " OR ClassNo=4";
            SheetNameString = "季巡檢 - 依每季";
        }
        if (s == 12 && m == 31) { //一年的最後一天
            tempString += " OR ClassNo=5";
            SheetNameString = "年度巡檢 - 依每年";
        }
        return tempString;
    }

    //從資料庫取得舊資料
    private List<String> getHistoryCheckList() {
        List<String> tempList = new ArrayList<>();
        //建立物件
        DataBaseName = "Inspection_System";
        DataBaseTable = "Decect_Result";
        DataBaseVersion = 2;
        sqlDataBaseHelper = new SqlDataBaseHelper(this, DataBaseName, null, DataBaseVersion, DataBaseTable);
        db = sqlDataBaseHelper.getWritableDatabase();//開啟資料庫
        //得到查詢條件(表單號碼)
        String di_no = bundle.getString("Plan");
        Cursor c = db.rawQuery("SELECT * FROM Detect_Result WHERE DI_No='" + di_no + "'", null);
        if (c.getCount() > 0) {
            c.moveToFirst(); //移到第一行
            do {        // 逐筆讀出資料
                tempList.add(c.getString(3));
            } while (c.moveToNext());    // 有一下筆就繼續迴圈
        }
        return tempList;
    }

    //按下送出按鈕後的事件處理，寫入到資料表
    public void submit(View view) {
        //檢查是否已完成表單
        boolean isFinished = haveFinishCheck();
        if (isFinished) {
            writeDetectResultTable();
            //寫入另一個資料表
            writeDetectInfoTable();
            //檢查是否是重複表單，重新設置編號
            dbCheckData();
            //重置ToggleButton
            //得到當前的ListView的ViewGroup
            ViewGroup v = (ViewGroup) (getWindow().getDecorView().findViewById(R.id.lv_itemCheck));
            int intcount = v.getChildCount();
            for (int i = 0; i < intcount; i++) {
                ToggleButton tb1 = v.getChildAt(i).findViewById(R.id.toggleButton);
                ToggleButton tb2 = v.getChildAt(i).findViewById(R.id.toggleButton2);
                tb1.setChecked(false);
                tb2.setChecked(false);
            }

        } else {
            Toast.makeText(getApplicationContext(), "尚未完成檢查", Toast.LENGTH_SHORT).show();
        }
    }

    public void writeDetectResultTable() {
        //建立物件，寫入Detect_Result資料表
        DataBaseName = "Inspection_System";
        DataBaseTable = "Detect_Result";
        DataBaseVersion = 2;
        sqlDataBaseHelper = new SqlDataBaseHelper(this, DataBaseName, null, DataBaseVersion, DataBaseTable);
        db = sqlDataBaseHelper.getWritableDatabase();//開啟資料庫
        //得到當前的ListView的ViewGroup
        ViewGroup v = (ViewGroup) (getWindow().getDecorView().findViewById(R.id.lv_itemCheck));
        int intcount = v.getChildCount();
        //寫入資料庫
        ContentValues contentValues = new ContentValues();
        for (int i = 0; i < intcount; i++) {
            ToggleButton tb1 = v.getChildAt(i).findViewById(R.id.toggleButton);
            ToggleButton tb2 = v.getChildAt(i).findViewById(R.id.toggleButton2);
            TextView tv = v.getChildAt(i).findViewById(R.id.textViewItem);
            contentValues.put("DI_No", tv_SheetNm.getText().toString());
            contentValues.put("Item_No", i + 1);
            contentValues.put("ItemName", tv.getText().toString());
            contentValues.put("ISOK", tb1.isChecked() ? 1 : 0);
            contentValues.put("IsRecord", tb2.isChecked() ? 1 : 0);
            if (tb2.isChecked()) {
                contentValues.put("RecordNo", tv_SheetNm.getText().toString() + "-" + i);
            } else {
                contentValues.put("RecordNo", "");
            }
            upLoadNo = db.insert(DataBaseTable, null, contentValues);
        }
        if (upLoadNo != 0 && upLoadNo != -1) {
            Toast.makeText(getApplicationContext(), "寫入" + intcount + "筆資料成功", Toast.LENGTH_SHORT).show();
            upLoadNo = 0;
        } else {
            Toast.makeText(getApplicationContext(), "寫入Detect_Result失敗", Toast.LENGTH_SHORT).show();
        }
        db.close();
    }

    //寫入到 Detect_Info 檢查總表
    public void writeDetectInfoTable() {
        //建立物件，寫入Detect_Result資料表
        DataBaseName = "Inspection_System";
        DataBaseTable = "Detect_Info";
        DataBaseVersion = 2;
        sqlDataBaseHelper = new SqlDataBaseHelper(this, DataBaseName, null, DataBaseVersion, DataBaseTable);
        db = sqlDataBaseHelper.getWritableDatabase();//開啟資料庫
        //查詢資料庫是否已排定本日的巡檢
        Cursor c = db.rawQuery("SELECT * FROM Detect_Info WHERE DateTime='" + s + "' and Is_Checked=0", null);
        c.moveToLast();
        int count = c.getCount();
        if (count != 0) {
            //更新資料庫
            ContentValues contentValues = new ContentValues();
            contentValues.put("Is_Checked", 1);
            upLoadNo = db.update(DataBaseTable, contentValues, "DateTime='" + s.toString() + "'", null);
        } else {
            //寫入資料庫，增加一筆資料
            ContentValues contentValues = new ContentValues();
            contentValues.put("DateTime", s.toString());
            contentValues.put("DI_Ser", sheetNumber);
            contentValues.put("NameID", NameID);
            contentValues.put("Is_Checked", 1);
            contentValues.put("SheetName", SheetNameString);
            contentValues.put("Parts", NoteActivity.parts); //將NoteActivity的參數parts取出
            upLoadNo = db.insert(DataBaseTable, null, contentValues);
        }

        if (upLoadNo != 0 && upLoadNo != -1) {
            Toast.makeText(getApplicationContext(), "寫入本地資料表Detect_Info成功", Toast.LENGTH_SHORT).show();
            upLoadNo = 0;
        } else {
            Toast.makeText(getApplicationContext(), "寫入Detect_Info失敗", Toast.LENGTH_SHORT).show();
        }
        db.close();
    }

    //檢查本地資料庫是否已有資料
    public boolean dbCheckData() {
        boolean haveDouble = false;
        String sheetTempNo = null;
        //建立物件，讀取Detect_Info資料表
        DataBaseName = "Inspection_System";
        DataBaseTable = "Detect_Info";
        DataBaseVersion = 2;
        sqlDataBaseHelper = new SqlDataBaseHelper(this, DataBaseName, null, DataBaseVersion, DataBaseTable);
        db = sqlDataBaseHelper.getWritableDatabase();//開啟資料庫
        Cursor c = db.rawQuery("SELECT * FROM Detect_Info WHERE DateTime='" + s + "' and Is_Checked=1", null);
        c.moveToLast();
        int count = c.getCount();
        if (count != 0) {
            haveDouble = true;
            c.moveToFirst();
            //得到最新的表單序號
            while (true) {
                sheetTempNo = c.getString(2);
                if (!c.moveToNext()) break;
            }
            s = DateFormat.format("yyyy-MM-dd", mCal.getTime());

            //如果是bundle傳過來的資料而且不是今天，則編號不更新
            if (bundle == null) {
                sheetNumber = Integer.parseInt(sheetTempNo) + 1; //產生新序號
                sheetNumberString = s.toString() + "-" + sheetNumber;
            } else {
                if ((DateFormat.format("yyyy-MM-dd", mCal.getTime())).toString().compareTo(DateFormat.format("yyyy-MM-dd", Calendar.getInstance().getTime()).toString()) == 0) {
                    sheetNumber = Integer.parseInt(sheetTempNo) + 1; //產生新序號
                    sheetNumberString = s.toString() + "-" + sheetNumber;
                } else {
                    sheetNumberString = s.toString() + "-" + sheetNumber;
                }

            }
            tv_SheetNm.setText(sheetNumberString);
        } else {
            haveDouble = false;
        }
        return haveDouble;
    }

    //檢查是否已完成表單
    public boolean haveFinishCheck() {
        boolean isFinished = false;
        //得到當前的ListView的ViewGroup
        ViewGroup v = (ViewGroup) (getWindow().getDecorView().findViewById(R.id.lv_itemCheck));
        int intcount = v.getChildCount();
        for (int i = 0; i < intcount; i++) {
            ToggleButton tb1 = v.getChildAt(i).findViewById(R.id.toggleButton);
            ToggleButton tb2 = v.getChildAt(i).findViewById(R.id.toggleButton2);
            if (tb1.isChecked() == false && tb2.isChecked() == false) {
                isFinished = false;
                break;
            } else {
                isFinished = true;
            }
        }
        return isFinished;
    }

    //檢查是否已有紀錄表單(如果有紀錄一筆圖片或文字)
    public boolean haveRecordCheck() {
        boolean isRecorded = false;
        //得到當前的ListView的ViewGroup
        ViewGroup v = (ViewGroup) (getWindow().getDecorView().findViewById(R.id.lv_itemCheck));
        int intcount = v.getChildCount();
        for (int i = 0; i < intcount; i++) {
            ToggleButton tb2 = v.getChildAt(i).findViewById(R.id.toggleButton2);
            if (tb2.isChecked() == true) {
                isRecorded = true;
                break;
            } else {
                isRecorded = false;
            }
        }
        return isRecorded;
    }

    //檢查資料庫是否有舊資料，如果有則更改畫面
    public void getDBdataToChangeView() {
        //讀取Detect_Result資料表
        DataBaseName = "Inspection_System";
        DataBaseTable = "Detect_Result";
        DataBaseVersion = 2;
        sqlDataBaseHelper = new SqlDataBaseHelper(this, DataBaseName, null, DataBaseVersion, DataBaseTable);
        db = sqlDataBaseHelper.getWritableDatabase();//開啟資料庫
        //得到當前的表單編號，用表單號碼查詢歷史清單
        time = tv_SheetNm.getText().toString();
        Cursor c = db.rawQuery("SELECT * FROM Detect_Result WHERE DI_No='" + time + "'", null);
        c.moveToLast();
        int count = c.getCount();
        if (count != 0) {
            //將 Cursor 移到第一行
            c.moveToFirst();
            //必須在執行序裡面等待時間，讓畫面產生(不太好的寫法，但是還能用)
            synchronized (updateView) {
                try {
                    updateView.wait(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            //得到當前的ListView的ViewGroup
            ViewGroup v = getWindow().getDecorView().findViewById(R.id.lv_itemCheck);
            int intcount = v.getChildCount();
            for (int i = 0; i < intcount; i++) {
                //View v = checkItemListView.getAdapter().getView(i, null, checkItemListView);
                firstTB.add(v.getChildAt(i).findViewById(R.id.toggleButton));
                secondTB.add(v.getChildAt(i).findViewById(R.id.toggleButton2));
                firstTBbool.add(c.getString(4).equals("1") ? true : false);
                secondTBbool.add(c.getString(5).equals("1") ? true : false);
                c.moveToNext();
            }
            c.close();
        } else {
            c.close();
            return;
        }
    }

}