package com.example.mysqlconnectactivity.check;

import static com.example.mysqlconnectactivity.sqlite.SqlDataBaseHelper.DataBaseName;
import static com.example.mysqlconnectactivity.sqlite.SqlDataBaseHelper.DataBaseTable;
import static com.example.mysqlconnectactivity.sqlite.SqlDataBaseHelper.DataBaseVersion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.mysqlconnectactivity.DetectInfoBean;
import com.example.mysqlconnectactivity.ListAdapter;
import com.example.mysqlconnectactivity.NeedCheckAdapter;
import com.example.mysqlconnectactivity.R;
import com.example.mysqlconnectactivity.databinding.ActivityCheckSheetBinding;
import com.example.mysqlconnectactivity.sqlite.SqlDataBaseHelper;
import com.example.mysqlconnectactivity.welcome.WelcomeActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CheckSheetActivity extends AppCompatActivity {
    private ActivityCheckSheetBinding binding;
    private BottomNavigationView bottomNavigationView;
    private ListView sheetListView;
    private List<String> descriptionList, plantimeList, statusList, dateTimeList, diSerList, isCheckedList, sheetNameList;
    private Calendar today;
    private CharSequence s;
    private NeedCheckAdapter myNeedCheckAdapter;
    public static Context context;
    //private DetectInfoBean detectInfoBean;
    private SqlDataBaseHelper sqlDataBaseHelper;
    private SQLiteDatabase db;
    private List<DetectInfoBean> myDetectInfoList;
    private NavigationEnum navigationEnum;

    //bottomNavigationView的Enum
    public enum NavigationEnum {
        NeedCheck, HistoryCheck
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_check_sheet);
        //得到實體
        binding = ActivityCheckSheetBinding.inflate(getLayoutInflater());
        context = this;
        bottomNavigationView = binding.bottomNavigationView2;
        sheetListView = binding.lvSheet;
        descriptionList = new ArrayList<>();
        plantimeList = new ArrayList<>();
        statusList = new ArrayList<>();
        dateTimeList = new ArrayList<>();
        isCheckedList = new ArrayList<>();
        sheetNameList = new ArrayList<>();
        diSerList = new ArrayList<>();
        today = Calendar.getInstance(); //得到今天的日期
        myDetectInfoList = new ArrayList<>();
        //產生布局
        setContentView(binding.getRoot());

        //產生ListView的資料
        for (int i = 0; i <= 30; i++) {
            today = Calendar.getInstance(); //得到今天的日期
            today.add(Calendar.DAY_OF_MONTH, i); //每天加1
            s = DateFormat.format("yyyy-MM-dd", today.getTime());
            if (today.get(Calendar.DAY_OF_MONTH) <= 31) {
                //要跳過星期六及星期日
                if (!(today.get(Calendar.DAY_OF_WEEK) == 1 || today.get(Calendar.DAY_OF_WEEK) == 7)) {
                    if (today.get(Calendar.MONTH) == 12 && today.get(Calendar.DAY_OF_MONTH) == 31) {
                        descriptionList.add("年度巡檢 - 依每年");
                    } else if (today.get(Calendar.DAY_OF_MONTH) == 1 && (today.get(Calendar.MONTH) == 4 || today.get(Calendar.MONTH) == 7 || today.get(Calendar.MONTH) == 10)) {
                        descriptionList.add("季巡檢 - 依每季");
                    } else if (today.get(Calendar.DAY_OF_MONTH) == 1) {
                        descriptionList.add("月巡檢 - 依每月");
                    } else if (today.get(Calendar.DAY_OF_WEEK) - 1 == 1) {
                        descriptionList.add("週巡檢 - 依每週");
                    } else {
                        descriptionList.add("日常巡檢 - 依每日");
                    }
                    plantimeList.add(s.toString());
                    statusList.add("待巡檢");
                }
            }
        }

        // ListView 中所需之資料參數可透過修改 Adapter 的建構子傳入(預先載入)
        myNeedCheckAdapter = new NeedCheckAdapter(CheckSheetActivity.this, descriptionList, plantimeList, statusList);
        //設定 ListView 的 Adapter
        sheetListView.setAdapter(myNeedCheckAdapter);

        //bottomNavigationView的事件處理
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.need_check:
                    navigationEnum = NavigationEnum.NeedCheck;
                    //連線資料庫，產生已經點檢的歷史項目
                    myDetectInfoList = checkHistory();
                    for (int i = 0; i < myDetectInfoList.size(); i++) {
                        dateTimeList.add(myDetectInfoList.get(i).getDateTime() + "-" + myDetectInfoList.get(i).getDiSer());
                        //diSerList.add(myDetectInfoList.get(i).getDiSer());
                        isCheckedList.add(myDetectInfoList.get(i).getIsChecked().equals("1") ? "已完成" : "未完成");
                        sheetNameList.add(myDetectInfoList.get(i).getSheetName());
                    }
                    // ListView 中所需之資料參數可透過修改 Adapter 的建構子傳入
                    myNeedCheckAdapter = new NeedCheckAdapter(CheckSheetActivity.this, descriptionList, plantimeList, statusList);
                    //設定 ListView 的 Adapter
                    sheetListView.setAdapter(myNeedCheckAdapter);
                    break;
                case R.id.no_check:
                    //先結束動畫
                    NeedCheckAdapter.tempView.clearAnimation();
                    //連線資料庫，產生已經點檢的歷史項目
                    myDetectInfoList = checkHistory();
                    for (int i = 0; i < myDetectInfoList.size(); i++) {
                        dateTimeList.add(myDetectInfoList.get(i).getDateTime() + "-" + myDetectInfoList.get(i).getDiSer());
                        //diSerList.add(myDetectInfoList.get(i).getDiSer());
                        isCheckedList.add(myDetectInfoList.get(i).getIsChecked().equals("1") ? "已完成" : "未完成");
                        sheetNameList.add(myDetectInfoList.get(i).getSheetName());
                    }
                    navigationEnum = NavigationEnum.HistoryCheck;
                    // ListView 中所需之資料參數可透過修改 Adapter 的建構子傳入
                    myNeedCheckAdapter = new NeedCheckAdapter(CheckSheetActivity.this, sheetNameList, dateTimeList, isCheckedList);
                    //設定 ListView 的 Adapter
                    sheetListView.setAdapter(myNeedCheckAdapter);
                    break;
            }
            return true;
        });

        //ListView的設置監聽器
        sheetListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                Bundle bundle = new Bundle(); //傳遞資料使用
                //得到當下點選ListView的Item，從ID得到當下設定的文字
                intent.setClass(context, CheckItemActivity.class);
                TextView tvTemp = view.findViewById(R.id.tv_plantime);
                //欲傳遞的資料
                bundle.putString("Plan", tvTemp.getText().toString());
                //判斷是否為已巡檢過的歷史表單
                if (navigationEnum == NavigationEnum.HistoryCheck) {
                    bundle.putBoolean("isOld", true);
                }
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    public void close(View v) {
        Intent intent = new Intent(this, WelcomeActivity.class);
        startActivity(intent);
    }

    private List<DetectInfoBean> checkHistory() {
        List<DetectInfoBean> beanList = new ArrayList<>();
        DataBaseName = "Inspection_System";
        DataBaseTable = "Detect_Info";
        DataBaseVersion = 2;
        sqlDataBaseHelper = new SqlDataBaseHelper(this, DataBaseName, null, DataBaseVersion, DataBaseTable);
        db = sqlDataBaseHelper.getWritableDatabase();//開啟資料庫
        Cursor c = db.rawQuery("SELECT * FROM Detect_Info", null);
        c.moveToLast();
        int count = c.getCount();
        if (c.getCount() > 0) {
            c.moveToFirst(); //移到第一行
            do {        // 逐筆讀出資料
                beanList.add(new DetectInfoBean(c.getString(1), c.getString(2), c.getString(3), c.getString(4), c.getString(5), c.getString(6)));
            } while (c.moveToNext());    // 有一下筆就繼續迴圈
        }
        return beanList;
    }
}