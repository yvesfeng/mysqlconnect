package com.example.mysqlconnectactivity.check;

import static com.example.mysqlconnectactivity.sqlite.SqlDataBaseHelper.DataBaseName;
import static com.example.mysqlconnectactivity.sqlite.SqlDataBaseHelper.DataBaseTable;
import static com.example.mysqlconnectactivity.sqlite.SqlDataBaseHelper.DataBaseVersion;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mysqlconnectactivity.ManagerSheetAdapter;
import com.example.mysqlconnectactivity.ManagerSheetBean;
import com.example.mysqlconnectactivity.databinding.ActivityManagerSheetBinding;
import com.example.mysqlconnectactivity.sqlite.SqlDataBaseHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ManagerSheetActivity extends AppCompatActivity {

    //宣告
    private ActivityManagerSheetBinding binding;
    private RecyclerView managerRecycle;
    private List<ManagerSheetBean> beanList = new ArrayList<ManagerSheetBean>();
    private SqlDataBaseHelper sqlDataBaseHelper;
    private static SQLiteDatabase db; //可以利用此類別物件去新增，查詢，修改，刪除資料表資料
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityManagerSheetBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        //實體化
        managerRecycle = binding.managerRecycleView;
        context = this;
        //從資料庫產生beanList的資料
        beanList = getBeanList();

        //設定recycleView的樣式
        managerRecycle.setLayoutManager(new LinearLayoutManager(context));
        //managerRecycle.setLayoutManager(new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL));
        managerRecycle.setAdapter(new ManagerSheetAdapter(this, beanList));
    }

    //從Sqlite資料庫拿取資料
    private List<ManagerSheetBean> getBeanList() {
        List<ManagerSheetBean> myListBaen = new ArrayList<>();
        String status = null;
        //建立物件，讀取Detect_Info資料表
        DataBaseName = "Inspection_System";
        DataBaseTable = "Detect_Info";
        DataBaseVersion = 2;
        sqlDataBaseHelper = new SqlDataBaseHelper(this, DataBaseName, null, DataBaseVersion, DataBaseTable);
        db = sqlDataBaseHelper.getWritableDatabase();//開啟資料庫
        Cursor c = db.rawQuery("SELECT * FROM Detect_Info WHERE Is_Checked=0", null);
        c.moveToLast();
        int count = c.getCount();
        if (count != 0) {
            c.moveToFirst();//游標移到第一行
            while (true) {
                ManagerSheetBean bean = new ManagerSheetBean();
                //判斷排檢日期是否已逾期
                Date today = new Date();
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                String tempString = dateFormat.format(today); //先將今天的日期去掉時間，轉成字串
                try {
                    today = dateFormat.parse(tempString); //再將字串轉回日期
                    Date arrangeDate = dateFormat.parse(c.getString(1));
                    if (today.before(arrangeDate) || today.equals(arrangeDate)) {
                        status = "未檢查";
                    } else {
                        status = "已逾期";
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                bean.setSheetName(c.getString(5));
                bean.setTime(c.getString(1));
                bean.setMember(c.getString(3));
                bean.setStatus(status);
                myListBaen.add(bean);
                if (!c.moveToNext()) break;
            }
        }
        return myListBaen;
    }
}