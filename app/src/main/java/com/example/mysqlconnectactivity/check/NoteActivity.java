package com.example.mysqlconnectactivity.check;

import static com.example.mysqlconnectactivity.sqlite.SqlDataBaseHelper.DataBaseName;
import static com.example.mysqlconnectactivity.sqlite.SqlDataBaseHelper.DataBaseTable;
import static com.example.mysqlconnectactivity.sqlite.SqlDataBaseHelper.DataBaseVersion;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.mysqlconnectactivity.R;
import com.example.mysqlconnectactivity.databinding.ActivityNoteBinding;
import com.example.mysqlconnectactivity.sqlite.SqlDataBaseHelper;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class NoteActivity extends AppCompatActivity {
    private ActivityNoteBinding binding;
    private Button cameraBt, uploadDB;
    public static final int CAMERA_PERM_CODE = 101;
    public static final int WRITE_REQ_CODE = 102;
    private ImageView leftImageView, rightImageView, leftDownImageView, rightDownImageView;
    public static int num;
    private Bundle bundle;
    private TextView checkItemNo, checkItemName;
    private List<String> imageList;
    private SqlDataBaseHelper sqlDataBaseHelper;
    private SQLiteDatabase db;
    private EditText multiTextComment, partsEditText, partstypeEditText, partsQuantityEditText;
    private long uploadNo;
    Context context;
    public static boolean HAVE_RESET_TOGGLEBUTTON = false;
    public static String checkItemNoString;
    public String number, fileName, partstype;
    public static String parts;
    private List<String> picArray;
    private List<String> commentArray;
    private Handler updateViewHandler;
    private File picPath, savePicPath;
    private boolean isPermissionPassed, isHistory;
    private int quantity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_note);
        //產生布局文件
        binding = ActivityNoteBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        context = this;
        //產生實體
        cameraBt = binding.btCamera;
        uploadDB = binding.btUploadDB;
        leftImageView = binding.ivLeft;
        rightImageView = binding.ivRight;
        leftDownImageView = binding.ivLeftDown;
        rightDownImageView = binding.ivRightDown;
        checkItemNo = binding.tvRecNo;
        checkItemName = binding.tvCheckItem;
        multiTextComment = binding.ettmComment;
        partsEditText = binding.partsEditText;
        partstypeEditText = binding.partsTypeEditText;
        partsQuantityEditText = binding.quantityEditText;
        num = 0;
        imageList = new ArrayList<>();
        commentArray = new ArrayList<>();
        picArray = new ArrayList<>();
        updateViewHandler = new Handler();
        bundle = this.getIntent().getExtras(); //接收ListAdapter.java Intent過來的資料

        checkItemNo.setText(bundle.getString("RecNo"));
        checkItemName.setText(bundle.getString("RecName"));
        isHistory = bundle.getBoolean("IsHistory");

        //用日期檢查是否為當日巡檢表，如果不是就無法上傳資料庫
        if (checkItemNo.getText().toString().substring(0, 10).compareTo(DateFormat.format("yyyy-MM-dd", Calendar.getInstance().getTime()).toString()) != 0 || isHistory == true) {
            //檢查資料庫有無資料，如果有舊資料則顯示於畫面
            Thread readDBthread = new Thread(readDB);
            readDBthread.start();
            //將按鈕設定成不顯示
            uploadDB.setVisibility(View.GONE);
            Toast.makeText(getApplicationContext(), "非當日巡檢表，不可上傳資料庫", Toast.LENGTH_LONG).show();
        }
        getPermission();
    }

    Runnable readDB = new Runnable() {
        @Override
        public void run() {
            //讀取資料庫資料
            ReadDBdata();
            //改變UI畫面
            if (commentArray.size() != 0) {
                updateViewHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (picArray.get(0) != null) {
                            for (int i = 0; i < picArray.size(); i++) {
                                if (i == 0) {
                                    leftImageView.setImageBitmap(getImg(picArray.get(i)));
                                }
                                if (i == 1) {
                                    rightImageView.setImageBitmap(getImg(picArray.get(i)));
                                }
                                if (i == 2) {
                                    leftDownImageView.setImageBitmap(getImg(picArray.get(i)));
                                }
                                if (i == 3) {
                                    rightDownImageView.setImageBitmap(getImg(picArray.get(i)));
                                }
                            }
                        }
                        multiTextComment.setText(commentArray.get(0));
                        partsEditText.setText(parts);
                        partstypeEditText.setText(partstype);
                        partsQuantityEditText.setText(String.valueOf(quantity));
                    }
                });
            }
        }
    };

    private void ReadDBdata() {
        //讀取Record資料表
        DataBaseName = "Inspection_System";
        DataBaseTable = "Record";
        DataBaseVersion = 2;
        sqlDataBaseHelper = new SqlDataBaseHelper(this, DataBaseName, null, DataBaseVersion, DataBaseTable);
        db = sqlDataBaseHelper.getWritableDatabase();//開啟資料庫
        //得到當前的表單編號
        number = checkItemNo.getText().toString();
        Cursor c = db.rawQuery("SELECT * FROM Record WHERE RecordNo='" + number + "'", null);
        c.moveToLast();
        int count = c.getCount();
        if (count != 0) {
            //將游標移到第一行
            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                picArray.add(c.getString(1));
                commentArray.add(c.getString(2));
                parts = c.getString(5);
                partstype = c.getString(6);
                quantity = c.getInt(7);
                c.moveToNext();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        Toast.makeText(getApplicationContext(), "onStop", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //回上一個Activity時強制寫入資料表(非舊資料)
        if (((imageList.size() != 0) || (multiTextComment.getText().toString().equals("") == false)) && uploadDB.getVisibility() != View.GONE) {
            writeToRecord();
        }
        Toast.makeText(getApplicationContext(), "onDestroy", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if ((imageList.size() == 0) && (multiTextComment.getText().toString().equals("") == true)) {
            HAVE_RESET_TOGGLEBUTTON = true;
            checkItemNoString = checkItemNo.getText().toString();
        }
    }

    //按下相機按鈕的事件處理，開啟相機
    public void getCamera(View v) {
        switch (num) {
            case 0:
                openCamera(R.id.ivLeft);
                break;
            case 1:
                openCamera(R.id.ivRight);
                break;
            case 2:
                openCamera(R.id.ivLeftDown);
                break;
            case 3:
                openCamera(R.id.ivRightDown);
                break;
        }
        num++;
        //重置num
        if (num == 4)
            num = 0;
    }

    private void getPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)) {
            //請求權限
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAMERA_PERM_CODE);
        } else {
            isPermissionPassed = true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 101) {
            if (permissions[0].equals(Manifest.permission.CAMERA)) {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    /**如果用戶同意*/
                    isPermissionPassed = true;
                } else {
                    /**如果用戶不同意*/
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this
                            , Manifest.permission.CAMERA)) {
                        getPermission();
                    }
                    Toast.makeText(this, "請允許使用相機權限，否則無法開啟相機！", Toast.LENGTH_SHORT).show();
                }
            }
            if (permissions[1].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                if (grantResults.length > 0 && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    /**如果用戶同意*/
                    isPermissionPassed = true;
                } else {
                    /**如果用戶不同意*/
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this
                            , Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        getPermission();
                    }
                    Toast.makeText(this, "請允許寫入記憶卡權限，否則無法儲存相片！", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }//onRequestPermissionsResult

    private void openCamera(int picCode) {
        if (isPermissionPassed) {
            Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(camera, picCode); //開啟相機並傳入 requestCode 請求關閉相機後的處理
        } else {
            getPermission();
        }
    }

    //相機關閉後依據請求碼處理程序的回呼函式
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            Bitmap image = (Bitmap) data.getExtras().get("data");
            //得到SD卡根目錄
            String sdRootPath = getSDPath();
            //相片儲存在SD卡跟目錄的 "Inspection" 資料夾下
            picPath = new File(sdRootPath, "Inspection");
            if (!picPath.exists()) {
                picPath.mkdir(); //建立資料夾
            }
            switch (requestCode) {
                case R.id.ivLeft:
                    leftImageView.setImageBitmap(image);
                    fileName = checkItemNo.getText().toString() + "-0.jpg";
                    savePicPath = new File(picPath, fileName);
                    try {
                        FileOutputStream fos = new FileOutputStream(savePicPath, false);
                        image.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                        fos.flush();
                        fos.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    imageList.add(savePicPath.toString());
                    break;
                case R.id.ivRight:
                    rightImageView.setImageBitmap(image);
                    fileName = checkItemNo.getText().toString() + "-1.jpg";
                    savePicPath = new File(picPath, fileName);
                    try {
                        FileOutputStream fos = new FileOutputStream(savePicPath, false);
                        image.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                        fos.flush();
                        fos.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    imageList.add(savePicPath.toString());
                    break;
                case R.id.ivLeftDown:
                    leftDownImageView.setImageBitmap(image);
                    fileName = checkItemNo.getText().toString() + "-2.jpg";
                    savePicPath = new File(picPath, fileName);
                    try {
                        FileOutputStream fos = new FileOutputStream(savePicPath, false);
                        image.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                        fos.flush();
                        fos.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    imageList.add(savePicPath.toString());
                    break;
                case R.id.ivRightDown:
                    rightDownImageView.setImageBitmap(image);
                    fileName = checkItemNo.getText().toString() + "-3.jpg";
                    savePicPath = new File(picPath, fileName);
                    try {
                        FileOutputStream fos = new FileOutputStream(savePicPath, false); // false是將檔案複寫
                        image.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                        fos.flush();
                        fos.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    imageList.add(savePicPath.toString());
                    break;
            }
        }
    }

    //資料上傳按鈕按下後的上傳資料庫
    public void uploadDB(View view) {
        writeToRecord();
        uploadDB.setVisibility(View.GONE);
    }

    public void writeToRecord() {
        //建立物件
        DataBaseName = "Inspection_System";
        DataBaseTable = "Record";
        DataBaseVersion = 2;
        sqlDataBaseHelper = new SqlDataBaseHelper(this, DataBaseName, null, DataBaseVersion, DataBaseTable);
        db = sqlDataBaseHelper.getWritableDatabase();//開啟資料庫
        //處理資料
        parts = partsEditText.getText().toString().equals("") ? "無" : partsEditText.getText().toString();
        partstype = partstypeEditText.getText().toString().equals("") ? "無" : partstypeEditText.getText().toString();
        quantity = partsQuantityEditText.getText().toString().trim().equals("") ? 0 : Integer.parseInt(partsQuantityEditText.getText().toString().trim());
        //寫入資料庫
        ContentValues contentValues = new ContentValues();
        if (!multiTextComment.getText().toString().isEmpty()) {
            if (imageList.size() != 0) {
                for (int i = 0; i < imageList.size(); i++) {
                    contentValues.put("RecordNo", checkItemNo.getText().toString());
                    contentValues.put("Picture", imageList.get(i));
                    contentValues.put("Comment", multiTextComment.getText().toString());
                    contentValues.put("PicNo", i);
                    contentValues.put("Parts", parts);
                    contentValues.put("PartsType", partstype);
                    contentValues.put("PartsQuantity", quantity);
                    db.insert(DataBaseTable, null, contentValues);
                    uploadNo++;
                }
            } else {
                contentValues.put("RecordNo", checkItemNo.getText().toString());
                contentValues.put("Comment", multiTextComment.getText().toString());
                contentValues.put("Parts", parts);
                contentValues.put("PartsType", partstype);
                contentValues.put("PartsQuantity", quantity);
                db.insert(DataBaseTable, null, contentValues);
                uploadNo = 1;
            }
        } else {
            Toast.makeText(context, "至少必須填入備註說明。", Toast.LENGTH_SHORT).show();
        }
        if (uploadNo != 0 || uploadNo != -1) {
            Toast.makeText(context, "上傳" + uploadNo + "筆資料成功", Toast.LENGTH_SHORT).show();
        } else if (!multiTextComment.getText().toString().isEmpty()) {
            Toast.makeText(context, "上傳失敗", Toast.LENGTH_SHORT).show();
        }
    }

    //讀取SD Card根目錄
    public static String getSDPath() {
        File sdDir = null;
        boolean sdCardExist = Environment.getExternalStorageState()
                .equals(android.os.Environment.MEDIA_MOUNTED);//判斷sd卡是否存在
        if (sdCardExist) {
            sdDir = Environment.getExternalStorageDirectory();//獲取跟目錄 storage/emulated/0
        }
        return sdDir.toString();
    }

    public Bitmap getImg(String path) {
        Bitmap bitmap = null;
        try {
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(path));
            bitmap = BitmapFactory.decodeStream(bis);
            bis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }


    //將圖片轉換成byte[]才能存入資料庫
//    public byte[] img(Bitmap bitmap) {
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
//        return baos.toByteArray();
//    }

    //將byte[]轉成Bitmap
//    public Drawable redoimg(byte[] b) {
//        Bitmap bm = BitmapFactory.decodeByteArray(b, 0, b.length); // bm的值會是null，因為sqlite的Cursor值最大是1MB，所以byte[]的值是不完整的
//        if (b.length != 0) {
//            BitmapDrawable bd = new BitmapDrawable(getResources(), bm);
//            return bd;
//        } else {
//            return null;
//        }
//    }
}