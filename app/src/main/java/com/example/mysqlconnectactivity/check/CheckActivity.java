package com.example.mysqlconnectactivity.check;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.format.DateFormat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import androidx.appcompat.app.AppCompatActivity;
import com.example.mysqlconnectactivity.ListAdapter;
import com.example.mysqlconnectactivity.R;
import com.example.mysqlconnectactivity.databinding.ActivityCheckBinding;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

//此頁面暫時沒用到，BottomNavigationView測試切換頁面資料儲存及顯示功能使用
public class CheckActivity extends AppCompatActivity {
    private ActivityCheckBinding binding;
    //private static final String Item_Text = "Item text";
    private ListView dayListView;
    private ListAdapter myListAdapter;
    private BottomNavigationView bottomNavigationView;
    private TextView textViewDate;
    private Context context;
    public List<Integer> daytbYesNoConditionList = new ArrayList<Integer>();
    public List<Integer> daytbCommentConditionList = new ArrayList<Integer>();
    public List<Integer> weektbYesNoConditionList = new ArrayList<Integer>();
    public List<Integer> weektbCommentConditionList = new ArrayList<Integer>();
    public List<Integer> monthtbYesNoConditionList = new ArrayList<Integer>();
    public List<Integer> monthtbCommentConditionList = new ArrayList<Integer>();
    public List<Integer> seasontbYesNoConditionList = new ArrayList<Integer>();
    public List<Integer> seasontbCommentConditionList = new ArrayList<Integer>();
    public List<Integer> yeartbYesNoConditionList = new ArrayList<Integer>();
    public List<Integer> yeartbCommentConditionList = new ArrayList<Integer>();
    public checkStatus check = checkStatus.Day;
    public Toast myToast;
    public myHandler handler;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new myHandler();
        //在build.gradle將 viewBinding 設為 true，則新增一個 view 就會自動產生一個 viewBinding.java的類別
        binding = ActivityCheckBinding.inflate(getLayoutInflater()); //產生實體
        //產生布局
        setContentView(binding.getRoot());
        bottomNavigationView = binding.bottomNavigationView;
        textViewDate = binding.textViewDate;
        //設定今天日期
        Calendar mCal = Calendar.getInstance();
        CharSequence s = DateFormat.format("yyyy-MM-dd kk:mm:ss", mCal.getTime());
        textViewDate.setText(s.toString());
        //建立ListView元件的實體
        dayListView = binding.dayList;
        //定義 DayListView 每個 Item 的資料
        List<String> dayList = new ArrayList<String>();
        dayList.add("門口外柵欄感測器是否正常運作?");
        dayList.add("門口外柵欄馬達是否正常運作?");
        dayList.add("門口內柵欄AI影像辨識是否正常?");
        dayList.add("門口內柵欄馬達是否正常?");
        dayList.add("接待大廳人臉辨識是否正常?");
        //定義 WeekListView 每個 Item 的資料
        List<String> weekList = new ArrayList<String>();
        weekList.add("氣壓儲槽壓力是否正常?");
        weekList.add("氣壓源是否正常?");
        weekList.add("氣壓主動力電壓是否正常?");
        weekList.add("廠房電源電壓是否正常?");
        weekList.add("主要電源保護迴路是否正常?");
        //定義 MonthListView 每個 Item 的資料
        List<String> monthList = new ArrayList<String>();
        monthList.add("門口柵欄感測器是否汙損?");
        monthList.add("柵欄電控箱保險絲是否正常?");
        monthList.add("接待大廳展示機是否正常?");
        monthList.add("門口柵欄AI攝影機鏡頭是否髒汙?");
        //定義 SeasonListView 每個 Item 的資料
        List<String> seasonList = new ArrayList<String>();
        seasonList.add("廠房接地是否正常?");
        seasonList.add("廠房氣壓管路是否破損?");
        seasonList.add("廠房主要電力供應線路是否老舊破損?");
        //定義 YearListView 每個 Item 的資料
        List<String> yearList = new ArrayList<String>();
        yearList.add("整廠消防灑水設施是否正常?");
        yearList.add("整廠煙霧偵測器是否正常?");
        yearList.add("消防滅火器是否過期?");
        // ListView 中所需之資料參數可透過修改 Adapter 的建構子傳入(畫面載入時先產生每日檢查事項)
        myListAdapter = new ListAdapter(CheckActivity.this, dayList);
        //設定 ListView 的 Adapter
        dayListView.setAdapter(myListAdapter);
        context = this;
        myToast = new Toast(context);
        //建立ListView點下選單的事件監聽
        dayListView.setOnItemClickListener(listViewOnItemClickListener);

        //bottomNavigationView的事件處理
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.get_day_check:
                    //BottomNavigation換頁時禁止自動跳轉頁面
                    ListAdapter.isRefused = true;
                    //bottomNavigationView換頁前先將當前頁面資料儲存方法
                    storeData();
                    //ListView 中所需之資料參數可透過修改 Adapter 的建構子傳入
                    myListAdapter = new ListAdapter(CheckActivity.this, dayList);
                    //設定 ListView 的 Adapter
                    dayListView.setAdapter(myListAdapter);
                    check = checkStatus.Day;
                    //更改螢幕顯示
                    handler.sendEmptyMessage(check.ordinal());
                    break;
                case R.id.get_week_check:
                    //BottomNavigation換頁時禁止自動跳轉頁面
                    ListAdapter.isRefused = true;
                    //bottomNavigationView換頁前先將當前頁面資料儲存方法
                    storeData();
                    // ListView 中所需之資料參數可透過修改 Adapter 的建構子傳入
                    myListAdapter = new ListAdapter(CheckActivity.this, weekList);
                    //設定 ListView 的 Adapter
                    dayListView.setAdapter(myListAdapter);
                    check = checkStatus.Week;
                    //更改螢幕顯示
                    handler.sendEmptyMessage(check.ordinal());
                    break;
                case R.id.get_month_check:
                    //BottomNavigation換頁時禁止自動跳轉頁面
                    ListAdapter.isRefused = true;
                    //bottomNavigationView換頁前先將當前頁面資料儲存方法
                    storeData();
                    // ListView 中所需之資料參數可透過修改 Adapter 的建構子傳入
                    myListAdapter = new ListAdapter(CheckActivity.this, monthList);
                    //設定 ListView 的 Adapter
                    dayListView.setAdapter(myListAdapter);
                    check = checkStatus.Month;
                    //更改螢幕顯示
                    handler.sendEmptyMessage(check.ordinal());
                    break;
                case R.id.get_season_check:
                    //BottomNavigation換頁時禁止自動跳轉頁面
                    ListAdapter.isRefused = true;
                    //bottomNavigationView換頁前先將當前頁面資料儲存方法
                    storeData();
                    // ListView 中所需之資料參數可透過修改 Adapter 的建構子傳入
                    myListAdapter = new ListAdapter(CheckActivity.this, seasonList);
                    //設定 ListView 的 Adapter
                    dayListView.setAdapter(myListAdapter);
                    check = checkStatus.Season;
                    //更改螢幕顯示
                    handler.sendEmptyMessage(check.ordinal());
                    break;
                case R.id.get_year_check:
                    //BottomNavigation換頁時禁止自動跳轉頁面
                    ListAdapter.isRefused = true;
                    //bottomNavigationView換頁前先將當前頁面資料儲存方法
                    storeData();
                    // ListView 中所需之資料參數可透過修改 Adapter 的建構子傳入
                    myListAdapter = new ListAdapter(CheckActivity.this, yearList);
                    //設定 ListView 的 Adapter
                    dayListView.setAdapter(myListAdapter);
                    check = checkStatus.Year;
                    //更改螢幕顯示
                    handler.sendEmptyMessage(check.ordinal());
                    break;
            }
            return true;
        });
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        myToast.makeText(context, "OnResume", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStart() {
        super.onStart();
        myToast.makeText(context, "OnStart", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        myToast.makeText(context, "onRestart", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onAttachedToWindow() {

    }

    //bottomNavigationView跳轉後的資料儲存方法
    public void storeData() {
        //得到當前的ListView的ViewGroup
        ViewGroup v = (ViewGroup) (getWindow().getDecorView().findViewById(R.id.dayList));
        int intcount = v.getChildCount();
        switch (check) {
            case Day:
                for (int i = 0; i < intcount; i++) {
                    ToggleButton tb1 = v.getChildAt(i).findViewById(R.id.toggleButton);
                    ToggleButton tb2 = v.getChildAt(i).findViewById(R.id.toggleButton2);
                    //紀錄狀態
                    daytbYesNoConditionList.add(tb1.isChecked() ? 1 : 0);
                    daytbCommentConditionList.add(tb2.isChecked() ? 1 : 0);
                }
                myToast.makeText(context, daytbYesNoConditionList.toString(), Toast.LENGTH_LONG).show();
                myToast.cancel();
                //myToast.makeText(context, daytbCommentConditionList.toString(), Toast.LENGTH_SHORT).show();
                break;
            case Week:
                for (int i = 0; i < intcount; i++) {
                    ToggleButton tb1 = v.getChildAt(i).findViewById(R.id.toggleButton);
                    ToggleButton tb2 = v.getChildAt(i).findViewById(R.id.toggleButton2);
                    //紀錄狀態
                    weektbYesNoConditionList.add(tb1.isChecked() ? 1 : 0);
                    weektbCommentConditionList.add(tb2.isChecked() ? 1 : 0);
                }
                myToast.makeText(context, weektbYesNoConditionList.toString(), Toast.LENGTH_LONG).show();
                myToast.cancel();
                //myToast.makeText(context, weektbCommentConditionList.toString(), Toast.LENGTH_SHORT).show();
                break;
            case Month:
                for (int i = 0; i < intcount; i++) {
                    ToggleButton tb1 = v.getChildAt(i).findViewById(R.id.toggleButton);
                    ToggleButton tb2 = v.getChildAt(i).findViewById(R.id.toggleButton2);
                    //紀錄狀態
                    monthtbYesNoConditionList.add(tb1.isChecked() ? 1 : 0);
                    monthtbCommentConditionList.add(tb2.isChecked() ? 1 : 0);
                }
                myToast.makeText(context, monthtbYesNoConditionList.toString(), Toast.LENGTH_LONG).show();
                myToast.cancel();
                //myToast.makeText(context, monthtbCommentConditionList.toString(), Toast.LENGTH_SHORT).show();
                break;
            case Season:
                for (int i = 0; i < intcount; i++) {
                    ToggleButton tb1 = v.getChildAt(i).findViewById(R.id.toggleButton);
                    ToggleButton tb2 = v.getChildAt(i).findViewById(R.id.toggleButton2);
                    //紀錄狀態
                    seasontbYesNoConditionList.add(tb1.isChecked() ? 1 : 0);
                    seasontbCommentConditionList.add(tb2.isChecked() ? 1 : 0);
                }
                myToast.makeText(context, seasontbYesNoConditionList.toString(), Toast.LENGTH_LONG).show();
                myToast.cancel();
                //myToast.makeText(context, monthtbCommentConditionList.toString(), Toast.LENGTH_SHORT).show();
                break;
            case Year:
                for (int i = 0; i < intcount; i++) {
                    ToggleButton tb1 = v.getChildAt(i).findViewById(R.id.toggleButton);
                    ToggleButton tb2 = v.getChildAt(i).findViewById(R.id.toggleButton2);
                    //紀錄狀態
                    yeartbYesNoConditionList.add(tb1.isChecked() ? 1 : 0);
                    yeartbCommentConditionList.add(tb2.isChecked() ? 1 : 0);
                }
                myToast.makeText(context, yeartbYesNoConditionList.toString(), Toast.LENGTH_LONG).show();
                myToast.cancel();
                //myToast.makeText(context, monthtbCommentConditionList.toString(), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    //處理bottomNavigationView切換後之資料回存並更改顯示
    public class myHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    //得到當前的ListView的ViewGroup
                    ViewGroup v = (ViewGroup) (getWindow().getDecorView().findViewById(R.id.dayList));
                    int intcount0 = v.getChildCount();
                    if (daytbYesNoConditionList.size() != 0 && daytbCommentConditionList.size() != 0) {
                        for (int i = 0; i < intcount0; i++) {
                            ToggleButton tb1 = v.getChildAt(i).findViewById(R.id.toggleButton);
                            ToggleButton tb2 = v.getChildAt(i).findViewById(R.id.toggleButton2);
                            //回復狀態
                            tb1.setChecked(daytbYesNoConditionList.get(i) == 1 ? true : false);
                            tb2.setChecked(daytbCommentConditionList.get(i) == 1 ? true : false);
                        }
                    }
                    //清空陣列資料
                    daytbYesNoConditionList.clear();
                    daytbCommentConditionList.clear();
                    //允許拍照之跳轉頁面
                    ListAdapter.isRefused = false;
                    break;
                case 1:
                    //得到當前的ListView的ViewGroup
                    ViewGroup v1 = (ViewGroup) (getWindow().getDecorView().findViewById(R.id.dayList));
                    int intcount1 = v1.getChildCount();
                    if (weektbYesNoConditionList.size() != 0 && weektbCommentConditionList.size() != 0) {
                        for (int i = 0; i < intcount1; i++) {
                            ToggleButton tb1 = v1.getChildAt(i).findViewById(R.id.toggleButton);
                            ToggleButton tb2 = v1.getChildAt(i).findViewById(R.id.toggleButton2);
                            //回復狀態
                            tb1.setChecked(weektbYesNoConditionList.get(i) == 1 ? true : false);
                            tb2.setChecked(weektbCommentConditionList.get(i) == 1 ? true : false);
                        }
                    }
                    //清空陣列資料
                    weektbYesNoConditionList.clear();
                    weektbCommentConditionList.clear();
                    //允許拍照之跳轉頁面
                    ListAdapter.isRefused = false;
                    break;
                case 2:
                    //得到當前的ListView的ViewGroup
                    ViewGroup v2 = (ViewGroup) (getWindow().getDecorView().findViewById(R.id.dayList));
                    int intcount2 = v2.getChildCount();
                    if (monthtbYesNoConditionList.size() != 0 && monthtbCommentConditionList.size() != 0) {
                        for (int i = 0; i < intcount2; i++) {
                            ToggleButton tb1 = v2.getChildAt(i).findViewById(R.id.toggleButton);
                            ToggleButton tb2 = v2.getChildAt(i).findViewById(R.id.toggleButton2);
                            //回復狀態
                            tb1.setChecked(monthtbYesNoConditionList.get(i) == 1 ? true : false);
                            tb2.setChecked(monthtbCommentConditionList.get(i) == 1 ? true : false);
                        }
                    }
                    //清空陣列資料
                    monthtbYesNoConditionList.clear();
                    monthtbCommentConditionList.clear();
                    //允許拍照之跳轉頁面
                    ListAdapter.isRefused = false;
                    break;
                case 3:
                    //得到當前的ListView的ViewGroup
                    ViewGroup v3 = (ViewGroup) (getWindow().getDecorView().findViewById(R.id.dayList));
                    int intcount3 = v3.getChildCount();
                    if (seasontbYesNoConditionList.size() != 0 && seasontbCommentConditionList.size() != 0) {
                        for (int i = 0; i < intcount3; i++) {
                            ToggleButton tb1 = v3.getChildAt(i).findViewById(R.id.toggleButton);
                            ToggleButton tb2 = v3.getChildAt(i).findViewById(R.id.toggleButton2);
                            //回復狀態
                            tb1.setChecked(seasontbYesNoConditionList.get(i) == 1 ? true : false);
                            tb2.setChecked(seasontbCommentConditionList.get(i) == 1 ? true : false);
                        }
                    }
                    //清空陣列資料
                    seasontbYesNoConditionList.clear();
                    seasontbCommentConditionList.clear();
                    //允許拍照之跳轉頁面
                    ListAdapter.isRefused = false;
                    break;
                case 4:
                    //得到當前的ListView的ViewGroup
                    ViewGroup v4 = (ViewGroup) (getWindow().getDecorView().findViewById(R.id.dayList));
                    int intcount4 = v4.getChildCount();
                    if (yeartbYesNoConditionList.size() != 0 && yeartbCommentConditionList.size() != 0) {
                        for (int i = 0; i < intcount4; i++) {
                            ToggleButton tb1 = v4.getChildAt(i).findViewById(R.id.toggleButton);
                            ToggleButton tb2 = v4.getChildAt(i).findViewById(R.id.toggleButton2);
                            //回復狀態
                            tb1.setChecked(yeartbYesNoConditionList.get(i) == 1 ? true : false);
                            tb2.setChecked(yeartbCommentConditionList.get(i) == 1 ? true : false);
                        }
                    }
                    //清空陣列資料
                    yeartbYesNoConditionList.clear();
                    yeartbCommentConditionList.clear();
                    //允許拍照之跳轉頁面
                    ListAdapter.isRefused = false;
                    break;
            }
        }
    }

    //宣告列舉的檢查主要分類
    public enum checkStatus {
        Day, Week, Month, Season, Year
    }

    //設定 Item 的 OnClick 事件，目前沒作用
    private AdapterView.OnItemClickListener listViewOnItemClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            ToggleButton tbIsComment = (ToggleButton) view.findViewById(R.id.toggleButton2);
            String isComment = tbIsComment.getText().toString();
            Toast.makeText(CheckActivity.this, position + ". " + isComment, Toast.LENGTH_SHORT).show();
        }
    };
}
