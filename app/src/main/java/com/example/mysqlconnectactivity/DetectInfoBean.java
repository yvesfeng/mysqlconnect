package com.example.mysqlconnectactivity;

public class DetectInfoBean {
    private String dateTime;
    private String diSer;
    private String nameID;
    private String isChecked;
    private String sheetName;
    private String parts;


    public DetectInfoBean(String date, String ser, String name, String checked, String sheet, String partname) {
        this.dateTime = date;
        this.diSer = ser;
        this.nameID = name;
        this.isChecked = checked;
        this.sheetName = sheet;
        this.parts = partname;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getNameID() {
        return nameID;
    }

    public void setNameID(String nameID) {
        this.nameID = nameID;
    }

    public String getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(String isChecked) {
        this.isChecked = isChecked;
    }

    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public String getDiSer() {
        return diSer;
    }

    public void setDiSer(String diSer) {
        this.diSer = diSer;
    }

    public String getParts() {
        return parts;
    }

    public void setParts(String parts) {
        this.parts = parts;
    }
}
