package com.example.mysqlconnectactivity.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class SqlDataBaseHelper extends SQLiteOpenHelper {
    public static String DataBaseName = "";
    public static int DataBaseVersion = 0;
    public static String DataBaseTable = "Account";
    public static boolean switchDB = false;

    //自動產生的建構子
    public SqlDataBaseHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version, String dataBaseTable) {
        super(context, name, factory, version);
        DataBaseName = name;
        DataBaseVersion = version;
        DataBaseTable = dataBaseTable;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //產生sql語句
        String SqlTable = "CREATE TABLE IF NOT EXISTS Account( " +
                "Ser integer PRIMARY KEY, " +
                "Account_Type integer, " +
                "ID text, " +
                "PWD text, " +
                "Enabled integer " +
                ");";
        String SqlTable2 = "CREATE TABLE IF NOT EXISTS Item( " +
                "id integer PRIMARY KEY AUTOINCREMENT, " +
                "Name text, " +
                "ClassNo text, " +
                "Enabled text" +
                ");";
        String SqlTable3 = "CREATE TABLE IF NOT EXISTS Detect_Result( " +
                "id integer PRIMARY KEY AUTOINCREMENT, " +
                "DI_No text, " +
                "Item_No int, " +
                "ItemName text, " +
                "ISOK int, " +
                "IsRecord int, " +
                "RecordNo text" +
                ");";
        String SqlTable4 = "CREATE TABLE IF NOT EXISTS Record( " +
                "id integer PRIMARY KEY AUTOINCREMENT, " +
                "Picture text, " +
                "Comment text, " +
                "RecordNo text, " +
                "PicNo int, " +
                "Parts text, "+
                "PartsType text, "+
                "PartsQuantity int"+
                ");";
        String SqlTable5 = "CREATE TABLE IF NOT EXISTS Detect_Info( " +
                "id integer PRIMARY KEY AUTOINCREMENT, " +
                "DateTime text, " +
                "DI_Ser int, " +
                "NameID text, " +
                "Is_Checked int, " +
                "SheetName text, " +
                "Parts text"+
                ");";
        db.execSQL(SqlTable);
        db.execSQL(SqlTable2);
        db.execSQL(SqlTable3);
        db.execSQL(SqlTable4);
        db.execSQL(SqlTable5);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//        if (db.getVersion() == 1) {
//            //移除資料表
//            final String SQL = "DROP TABLE User_Info";
//            db.execSQL(SQL);
//        }
        if (db.getVersion() == 2) {
            //移除資料表
            final String _SQL = "DROP TABLE Account";
            final String SQL = "DROP TABLE Item";
            final String SQL1 = "DROP TABLE Detect_Info";
            final String SQL2 = "DROP TABLE Detect_Result";
            final String SQL3 = "DROP TABLE Record";
            db.execSQL(_SQL);
            db.execSQL(SQL);
            db.execSQL(SQL1);
            db.execSQL(SQL2);
            db.execSQL(SQL3);
        }
    }
}
