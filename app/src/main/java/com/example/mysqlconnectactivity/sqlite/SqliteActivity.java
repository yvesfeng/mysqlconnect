package com.example.mysqlconnectactivity.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mysqlconnectactivity.databinding.ActivitySqliteBinding;

import java.sql.ResultSet;
import java.sql.SQLException;

//網路程式，了解Sqlite功能使用
public class SqliteActivity extends AppCompatActivity {
    private static final String DataBaseName = "WorkRecord";
    private static final int DataBaseVersion = 1;
    private static String DataBaseTable = "User_Info";
    private static SQLiteDatabase db;
    private SqlDataBaseHelper sqlDataBaseHelper; //自己建立產生資料庫及資料表的類別
    private ActivitySqliteBinding binding;
    private Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySqliteBinding.inflate(getLayoutInflater()); //產生實體
        //產生布局
        setContentView(binding.getRoot());
        //建立物件
        sqlDataBaseHelper = new SqlDataBaseHelper(this.context, DataBaseName, null, DataBaseVersion, DataBaseTable);
        db = sqlDataBaseHelper.getWritableDatabase();//開啟資料庫
    }

    //查詢
    public void select() {
        String[] ID, PWD;
        Cursor c = db.rawQuery("SELECT * FROM " + DataBaseTable, null);
        //產生陣列實體
        ID = new String[c.getCount()];
        PWD = new String[c.getCount()];
        //移到第一列，若資料為空，會回傳false
        c.moveToFirst();
        for (int i = 0; i < c.getCount(); i++) {
            ID[i] = c.getString(0);
            PWD[i] = c.getString(1);
            c.moveToNext();
        }
    }

    //新增
    public void insert(String id, String pwd) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("UI_ID", id);
        contentValues.put("UI_PW", pwd);
        db.insert(DataBaseTable, null, contentValues);
    }

    //更新
    public void update(String id) {
        int count;
        ContentValues contentValues = new ContentValues();
        //contentValues.put("PWD", );
        count = db.update(DataBaseTable, contentValues, "ID=" + id, null);
    }

    //更新整個資料表
    public void updateAll(ResultSet set) {
        int count;
        ContentValues contentValues = new ContentValues();
        //先移除所有資料
        db.delete(DataBaseTable, null, null);
        while (true) {
            try {
                if (!set.next()) break;
                else {
                    contentValues.put("UI_ID", set.getString("UI_ID"));
                    contentValues.put("UI_PW", set.getString("UI_PW"));
                    db.insert(DataBaseTable, null, contentValues);
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    //刪除
    public void delete(String id) {
        int count;
        count = db.delete(DataBaseTable, "ID=" + id, null);
    }
}
