package com.example.mysqlconnectactivity.welcome;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.mysqlconnectactivity.check.CheckItemActivity;
import com.example.mysqlconnectactivity.check.CheckSheetActivity;
import com.example.mysqlconnectactivity.check.ManagerSheetActivity;
import com.example.mysqlconnectactivity.databinding.ActivityWelcomeBinding;

import java.util.Calendar;

public class WelcomeActivity extends AppCompatActivity {
    //宣告
    private ActivityWelcomeBinding binding;
    private LinearLayout leftLinearLayout,downLeftLinearLayout;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_welcome);
        binding = ActivityWelcomeBinding.inflate(getLayoutInflater());
        //產生布局
        setContentView(binding.getRoot());

        //產生實體
        leftLinearLayout = binding.leftlayout;
        downLeftLinearLayout = binding.downLeft;
        context = this;
        //設置監聽器
        leftLinearLayout.setOnClickListener(onLeftClickListener);
        downLeftLinearLayout.setOnClickListener(onDownLeftClickListener);
    }

    private View.OnClickListener onLeftClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, CheckItemActivity.class);
            startActivity(intent);
        }
    };
    private View.OnClickListener onDownLeftClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, ManagerSheetActivity.class);
            startActivity(intent);
        }
    };
}