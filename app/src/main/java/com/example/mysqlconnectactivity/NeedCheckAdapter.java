package com.example.mysqlconnectactivity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mysqlconnectactivity.check.CheckSheetActivity;
import com.example.mysqlconnectactivity.ui.login.LoginActivity;

import java.util.List;

public class NeedCheckAdapter extends BaseAdapter {
    private LayoutInflater needCheckLayInf;
    private Context context;
    private List<String> descriptList, planList, stateList;
    private myViewHolder holder;
    private View myview;
    public static View tempView;
    private Animation remind;

    public NeedCheckAdapter(Context mycontext, List<String> mydescriptList, List<String> myplanList, List<String> mystateList) {
        //將傳進來的 mycontext 轉成layoutInflater
        needCheckLayInf = (LayoutInflater) mycontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        context = mycontext;
        descriptList = mydescriptList;
        planList = myplanList;
        stateList = mystateList;
    }

    //return的值一定不能是0，不然不會顯示ListView
    @Override
    public int getCount() {
        return descriptList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            //設定與回傳 convertView 作為顯示在這個 position 位置的 Item 的 View。
            convertView = needCheckLayInf.inflate(R.layout.list_item_needcheck, parent, false);
            //得到 holder物件實體
            holder = new NeedCheckAdapter.myViewHolder(convertView);
            //指派給holder的屬性
            holder.textViewDescription = convertView.findViewById(R.id.tv_description);
            holder.textViewPlanTime = convertView.findViewById(R.id.tv_plantime);
            holder.textViewStatus = convertView.findViewById(R.id.tv_status);
            convertView.setTag(holder);
            myview = convertView;
        } else {
            holder = (NeedCheckAdapter.myViewHolder) convertView.getTag();
            myview = convertView;
        }
        //賦值
        holder.textViewDescription.setText(descriptList.get(position));
        holder.textViewStatus.setText(stateList.get(position));
        holder.textViewPlanTime.setText(planList.get(position));

        //第一筆資料呈現動畫提醒用戶該巡檢的項目
        if (position == 0 && holder.textViewStatus.getText().toString().equals("待巡檢")) {
            remind = AnimationUtils.loadAnimation(new CheckSheetActivity().context, R.anim.erroralpha);
            tempView = myview;
            tempView.startAnimation(remind); //開啟動畫
        }
        return convertView;
    }

    public class myViewHolder extends RecyclerView.ViewHolder {

        public myViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        public TextView textViewDescription;
        public TextView textViewPlanTime;
        public TextView textViewStatus;
    }
}
