package com.example.mysqlconnectactivity.data;

import static com.example.mysqlconnectactivity.sqlite.SqlDataBaseHelper.DataBaseName;
import static com.example.mysqlconnectactivity.sqlite.SqlDataBaseHelper.DataBaseTable;
import static com.example.mysqlconnectactivity.sqlite.SqlDataBaseHelper.DataBaseVersion;
import static com.example.mysqlconnectactivity.ui.login.LoginActivity.context;
import static com.example.mysqlconnectactivity.ui.login.LoginActivity.pwd;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Message;

import com.example.mysqlconnectactivity.Crypto;
import com.example.mysqlconnectactivity.data.model.LoggedInUser;
import com.example.mysqlconnectactivity.sqlite.SqlDataBaseHelper;
import com.example.mysqlconnectactivity.ui.login.LoginActivity;

import java.io.IOException;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {
    public static String userName;
    public SqlDataBaseHelper sqlDataBaseHelper;
    public SQLiteDatabase db;

    //實際檢查資料表是否有此使用者和密碼
    public Result<LoggedInUser> login(String username, String password) {

        try {
            // TODO: handle loggedInUser authentication
            LoggedInUser myUser = new LoggedInUser(username, password);
            userName = myUser.getUserId();
            //從資料庫搜尋
            searchUserMethod();
            if (pwd == null) {
                //測試用帳號密碼
                if (myUser.getUserId().compareTo("raden") == 0 && myUser.getDisplayName().compareTo("super23277176") == 0) {
                    return new Result.Success<>(myUser);
                } else if (myUser.getUserId().compareTo("raden") == 0 && myUser.getDisplayName().compareTo("super23277176") != 0) {
                    return new Result.Error(new IOException("密碼錯誤"));
                } else {
                    return new Result.Error(new IOException("沒有這個使用者"));
                }
            } else if (Crypto.verifyHashedPassword(pwd, myUser.getDisplayName())) {
                return new Result.Success<>(myUser);
            } else {
                return new Result.Error(new IOException("密碼錯誤"));
            }
        } catch (Exception e) {
            return new Result.Error(new IOException("Error logging in", e));
        }
    }
    public void logout() {
        // TODO: revoke authentication
    }

    //region 資料庫搜尋使用者的執行序
    public void searchUser() {
        Thread searcgUserThread = new Thread(searcgUserRun);
        searcgUserThread.start();
    }

    Runnable searcgUserRun = new Runnable() {
        @Override
        public void run() {
            searchUserMethod();
        }
    };

    public void searchUserMethod() {
        //建立Sqlite資料庫物件
        DataBaseName = "Inspection_System";
        DataBaseTable = "Account";
        DataBaseVersion = 2;
        //context = new LoginActivity();
        sqlDataBaseHelper = new SqlDataBaseHelper(context, DataBaseName, null, DataBaseVersion, DataBaseTable);
        try {
            db = sqlDataBaseHelper.getWritableDatabase();//開啟資料庫
            Cursor c = db.rawQuery("SELECT * FROM Account Where ID='" + userName + "' and Enabled=1", null);
            c.moveToLast();
            if (c.getCount() == 0) {
                pwd = null;
            } else {
                c.moveToFirst();
                while (true) {
                    pwd = c.getString(3);
                    break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        db.close();
    }
    //endregion
}