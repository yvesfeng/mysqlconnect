package com.example.mysqlconnectactivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.example.mysqlconnectactivity.check.CheckItemActivity;
import com.example.mysqlconnectactivity.check.NoteActivity;

import java.util.ArrayList;
import java.util.List;


public class ListAdapter extends BaseAdapter {
    private LayoutInflater myLayInf;
    //List<Map<Integer, String>> myItemList;
    List<String> myItemList;
    private Context context;
    public View myview;
    public myViewHolder holder;
    public static boolean isRefused = false; //靜態屬性用來判斷是否需要跳轉到拍照頁面

    public ListAdapter(Context maincontext, List<String> itemlist) {
        //得到itemlist的layoutInflater
        myLayInf = (LayoutInflater) maincontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        myItemList = itemlist;
        context = maincontext;
    }

    @Override
    public int getCount() {
        //取得 ListView 列表 Item 的數量
        return myItemList.size();
    }

    @Override
    public Object getItem(int position) {
        //取得 ListView 列表於 position 位置上的 Item
        return myItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        //取得 ListView 列表於 position 位置上的 Item 的 ID
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            //設定與回傳 convertView 作為顯示在這個 position 位置的 Item 的 View。
            convertView = myLayInf.inflate(R.layout.list_item_daycheck, parent, false);
            holder = new myViewHolder(convertView);
            holder.textViewNm = convertView.findViewById(R.id.textViewNm);
            holder.textViewItem = convertView.findViewById(R.id.textViewItem);
            holder.tbYesNo = convertView.findViewById(R.id.toggleButton);
            holder.tbComment = convertView.findViewById(R.id.toggleButton2);
            convertView.setTag(holder);
            myview = convertView;
        } else {
            holder = (myViewHolder) convertView.getTag();
            myview = convertView;
        }
        //賦值
        holder.textViewNm.setText(String.valueOf(position + 1) + ".");
        holder.textViewItem.setText(myItemList.get(position));

        //設置ToggleButton的按鈕切換事件
        holder.tbComment.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    buttonView.setChecked(true);
                    //因為BottomNavigation切換頁面時會自動呼叫此按鈕事件，如果isRefused為true，則禁止自動跳轉頁面。
                    if (isRefused == false) {
                        //Toast.makeText(context, "跳轉頁面.....", Toast.LENGTH_SHORT).show();
                        //得到父Activity的表單編號
                        String sheetNoString = CheckItemActivity.tv_SheetNm.getText().toString();
                        //產生紀錄編號
                        String recordNoString = sheetNoString + "-" + position;
                        Intent intent = new Intent();
                        Bundle bundle = new Bundle();
                        intent.setClass(context, NoteActivity.class);
                        bundle.putString("RecNo",recordNoString);
                        bundle.putString("RecName",myItemList.get(position).toString());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }else{
                        isRefused = false; //回復成為允許跳轉狀態
                    }
                } else {
                    isRefused = false;
                    buttonView.setChecked(false);
                }
            }
        });
        holder.tbYesNo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    buttonView.setChecked(true);
                    Toast.makeText(context, "檢查OK", Toast.LENGTH_SHORT).show();
                } else {
                    buttonView.setChecked(false);
                    Toast.makeText(context, "檢查不OK", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return convertView;
    }
    public class myViewHolder extends ViewHolder {

        public myViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        public TextView textViewNm;
        public TextView textViewItem;
        public ToggleButton tbYesNo;
        public ToggleButton tbComment;
    }

}

