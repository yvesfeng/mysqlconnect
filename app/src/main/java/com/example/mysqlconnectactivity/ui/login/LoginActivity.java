package com.example.mysqlconnectactivity.ui.login;

import static com.example.mysqlconnectactivity.MysqlCon.haveConnectDB;
import static com.example.mysqlconnectactivity.data.LoginDataSource.userName;
import static com.example.mysqlconnectactivity.sqlite.SqlDataBaseHelper.DataBaseName;
import static com.example.mysqlconnectactivity.sqlite.SqlDataBaseHelper.DataBaseTable;
import static com.example.mysqlconnectactivity.sqlite.SqlDataBaseHelper.DataBaseVersion;
import static com.example.mysqlconnectactivity.sqlite.SqlDataBaseHelper.switchDB;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.mysqlconnectactivity.MysqlCon;
import com.example.mysqlconnectactivity.R;
import com.example.mysqlconnectactivity.check.CheckSheetActivity;
import com.example.mysqlconnectactivity.check.NoteActivity;
import com.example.mysqlconnectactivity.databinding.ActivityLoginBinding;
import com.example.mysqlconnectactivity.sqlite.SqlDataBaseHelper;
import com.example.mysqlconnectactivity.welcome.WelcomeActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import jcifs.smb.SmbFileOutputStream;

public class LoginActivity extends AppCompatActivity {
    private LoginViewModel loginViewModel;
    private ActivityLoginBinding binding;
    private Button connect, loginButton;
    private static TextView showData;
    private boolean hasRight, isPermissionPassed;
    private static SQLiteDatabase db; //可以利用此類別物件去新增，查詢，修改，刪除資料表資料
    private SqlDataBaseHelper sqlDataBaseHelper; //自己建立產生資料庫及資料表的類別
    public static MyHandler myHandler = new MyHandler();
    public static ProgressBar loadingProgressBar;
    public static long pg = 0;
    private Thread thread;
    public static long count = 0; //寫入資料庫比數
    public static long totalRow = 0;//總比數
    public static DataBaseTableName tableName = DataBaseTableName.Account;
    public static Context context;
    //private boolean isDownload = false;
    private Button uploadBT;
    private boolean isOK, mySQLisBig;
    private NetworkInfo networkInfo;
    private BroadcastReceiver mConnReceiver; //廣播接收物件，檢查網路連線使用
    private TextView reminderTV;
    private Animation alpha;
    private SharedPreferences mySharedPreferences; //當APP關閉時可以儲存資料，待下次開啟APP時使用
    public int failDownloadCount = 0;
    //使用JCIFS的套件，連線遠端的共享資料夾
    private NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication("smb://192.168.1.30", "yves", "yves0901");
    public static String pwd = null;

    //region 給進度條使用，更改主UI畫面
    public static class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    pg = (long) ((count * 100.0 / totalRow));
                    loadingProgressBar.setProgress((int) pg);    //  給 ProgressBar 當前的進度值
                    showData.setText(pg + "%");  // 顯示當時的進度值
                    break;
                case 2:
                    loadingProgressBar.setProgress(100);
                    //要有等待時間，下一個進度條才會正常顯示
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    break;
                case 3:
                    loadingProgressBar.setProgress(100);
                    showData.setTextSize(25.0f);
                    showData.setText("已下載 100%");
                    break;
                case 4:
                    loadingProgressBar.setProgress(100);
                    showData.setTextSize(25.0f);
                    showData.setText("已上傳 100%");
                    break;
            }
        }
    }
    //endregion

    //region 用以區別不同資料庫的列舉
    public enum DataBaseTableName {
        Account, Item, Detect_Info, Record, Detect_Result
    }
    //endregion

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //在build.gradle將 viewBinding 設為 true，則新增一個 view 就會自動產生一個 viewBinding.java的類別
        binding = ActivityLoginBinding.inflate(getLayoutInflater()); //產生實體
        //產生布局
        setContentView(binding.getRoot());
        //產生實體物件
        final EditText usernameEditText = binding.username;
        final EditText passwordEditText = binding.password;
        loginButton = binding.login;
        loadingProgressBar = binding.loading;
        uploadBT = binding.btUpload;
        connect = binding.connect;
        showData = binding.textView;
        loadingProgressBar.setVisibility(View.GONE);
        context = this;
        reminderTV = binding.tvReminder;
        alpha = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.erroralpha);
        mySharedPreferences = getSharedPreferences("isFinished", MODE_PRIVATE); //新增儲存資料實體
        // 當使用者開啟或關閉網路時會進入這邊
        mConnReceiver = new BroadcastReceiver() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onReceive(Context context, Intent intent) {
                // 判斷目前有無網路
                if (isNetworkAvailable()) {
                    // 以連線至網路，做更新資料等事情
                    reminderTV.setVisibility(View.INVISIBLE);
                    reminderTV.clearAnimation(); //結束動畫
                    Toast.makeText(LoginActivity.this, "有網路!", Toast.LENGTH_SHORT).show();
                } else {
                    // 沒有網路
                    reminderTV.setText("無網路連線");
                    reminderTV.setVisibility(View.VISIBLE);
                    reminderTV.startAnimation(alpha); //開啟動畫
                    Toast.makeText(LoginActivity.this, "沒網路!", Toast.LENGTH_SHORT).show();
                }
            }
        };

        //設定背景圖片透明度
        View v = findViewById(R.id.container);//container是設透明背景的layout 的id
        v.getBackground().setAlpha(90);//0~255透明度值

        //new ViewModelProvider會回傳 ViewModel
        loginViewModel = new ViewModelProvider(this, new LoginViewModelFactory())
                .get(LoginViewModel.class);

        //region 得到使用者輸入的資訊，loginFormState會判斷是否格式正確
        loginViewModel.getLoginFormState().observe(this, new Observer<LoginFormState>() {
            @Override
            public void onChanged(@Nullable LoginFormState loginFormState) {
                if (loginFormState == null) {
                    return;
                }
                loginButton.setEnabled(loginFormState.isDataValid());
                if (loginFormState.getUsernameError() != null) {
                    usernameEditText.setError(getString(loginFormState.getUsernameError()));
                }
                if (loginFormState.getPasswordError() != null) {
                    passwordEditText.setError(getString(loginFormState.getPasswordError()));
                }
            }
        });
        //endregion

        //region 得到使用者輸入的資訊，loginResult會判斷是否資料正確
        loginViewModel.getLoginResult().observe(this, new Observer<LoginResult>() {
            @Override
            public void onChanged(@Nullable LoginResult loginResult) {
                if (loginResult == null) {
                    return;
                }
                loadingProgressBar.setVisibility(View.GONE);
                showData.setText("");
                if (loginResult.getError() != null) {
                    showLoginFailed(loginResult.getError());
                }
                if (loginResult.getSuccess() != null) {
                    loginButton.setEnabled(false);
                    //執行下載資料庫程序
                    loadingProgressBar.setVisibility(View.VISIBLE);
                    //呼叫 mutiThread 執行序
                    thread = new Thread(mutiThread);
                    thread.start();
                    updateUiWithUser(loginResult.getSuccess());
                }
                setResult(Activity.RESULT_OK);
                //Complete and destroy login activity once successful

            }
        });
        //endregion

        //region 用來做使用者輸入檢查使用的監聽器
        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                loginViewModel.loginDataChanged(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());
            }
        };

        //設置監聽器
        usernameEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.addTextChangedListener(afterTextChangedListener);
        //設置密碼輸入完成後點選Enter鍵的監聽器
//        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (actionId == EditorInfo.IME_ACTION_DONE) {
//                    loginViewModel.login(usernameEditText.getText().toString(),
//                            passwordEditText.getText().toString());
//                }
//                return false; //返回true保留螢幕鍵盤
//            }
//        });
        //endregion

        //region 設置登入按鈕的監聽器
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkAvailable()) {
                    loginViewModel.login(usernameEditText.getText().toString(),
                            passwordEditText.getText().toString());
                } else {
                    Toast.makeText(getApplicationContext(), "無法連上伺服器，請檢查網路連線", Toast.LENGTH_LONG).show();
                }
            }
        });
        //endregion

        //region 檢查權限
        getPermission();
        //endregion

    } // end onCreate()

    //登入成功後畫面的顯示
    private void updateUiWithUser(LoggedInUserView model) {
        String welcome = getString(R.string.welcome) + model.getDisplayName();
        // TODO : initiate successful logged in experience
        Toast.makeText(getApplicationContext(), welcome, Toast.LENGTH_LONG).show();
    }

    //登入失敗後畫面的顯示
    private void showLoginFailed(@StringRes Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }

    @Override //給畫面切回這個Activity時做檢查使用
    protected void onRestart() {
        super.onRestart();
    }

    //當APP onCreate()及onStop()之後都會執行的方法
    @Override
    protected void onStart() {
        super.onStart();
        if (isNetworkAvailable()) {
            //檢查本地資料庫資料是否已上傳遠端資料庫
            thread = new Thread(dbCheckThread);
            thread.start();
        }
    }

    //當APP onPaused 和 onStop()之後重新回到APP，都會呼叫這個方法
    @Override
    protected void onResume() {
        super.onResume();
        // 註冊mConnReceiver，並用IntentFilter設置接收的事件類型為網路開關
        this.registerReceiver(mConnReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    public void onPause() {
        super.onPause();
        // 解除註冊
        this.unregisterReceiver(mConnReceiver);
    }

    @Override
    public void onStop() {
        super.onStop();
        //Toast.makeText(this, "onStop", Toast.LENGTH_LONG).show();
        // region 檢查MySQL的資料是不是最新的，如果是本機的資料比較新則條件成立，可以刪除MySQL資料表 (暫時先不加，視APP執行狀況而定)
//        Thread checkThread = new Thread(dbCheckThread);
//        checkThread.start();

//        if (mySQLisBig == false) {
//            //讀取上傳狀態是否完成
//            isFinished = mySharedPreferences.getBoolean("isFinished", false);
//            Toast.makeText(this, "onStop " + isFinished, Toast.LENGTH_SHORT).show();
//            if (isFinished == false) {
//                //刪除遠端資料表Detect_Info
//                //建立遠端資料庫實體物件
//                MysqlCon con = new MysqlCon("Inspection_System");
//                try {
//                    con = (MysqlCon) DriverManager.getConnection(con.url, con.db_user, con.db_password);
//                    Statement st = ((Connection) con).createStatement();
//                    st.execute("DELETE FROM Detect_Info");
//                    st.execute("DELETE FROM Detect_Result");
//                    st.execute("DELETE FROM Record");
//                } catch (SQLException throwables) {
//                    throwables.printStackTrace();
//                }
//            }
//        }
        //endregion
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //Toast.makeText(this, "onDestroy", Toast.LENGTH_SHORT).show();
    }

    // 回傳目前是否已連線至網路
    public boolean isNetworkAvailable() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        networkInfo = cm.getActiveNetworkInfo();

        return networkInfo != null &&
                networkInfo.isConnected();
    }


    // region 比對資料庫的資料(目前只比對Detect_Result一張資料表)，如果是本機資料庫比較新，則顯示上傳按鈕(連線Mysql資料庫必須寫在執行序)
    public Runnable dbCheckThread = new Runnable() {
        @Override
        public void run() {
            //連線程式
            mySQLisBig = dbCheck();
        }
    };

    public boolean dbCheck() {
        int countMysqlRow = 0;
        int countSqliteRow = 0;
        boolean mySQLisBig = false;
        //建立Sqlite資料庫物件
        DataBaseName = "Inspection_System";
        DataBaseTable = "Detect_Result";
        DataBaseVersion = 2;
        sqlDataBaseHelper = new SqlDataBaseHelper(this, DataBaseName, null, DataBaseVersion, DataBaseTable);
        db = sqlDataBaseHelper.getWritableDatabase();//開啟資料庫
        try {
            Cursor c = db.rawQuery("SELECT * FROM Detect_Result", null);
            c.moveToLast();
            countSqliteRow = c.getCount();
        } catch (Exception e) {
            e.printStackTrace();
        }
        db.close();
        //建立遠端資料庫實體物件
        MysqlCon con = new MysqlCon(DataBaseName);
        con.run();
        ResultSet set = con.getDataResultSet("SELECT * FROM Detect_Result");
        try {
            if (set != null) {
                set.last();
                countMysqlRow = set.getRow();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        if (countSqliteRow > countMysqlRow) { //本機資料筆數比遠端還要多，需要上傳
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loginButton.setEnabled(false);
                    uploadBT.setVisibility(View.VISIBLE);
                }
            });
            mySQLisBig = false;
        } else if (countSqliteRow == countMysqlRow) { //本機資料筆數和遠端一樣多
            mySQLisBig = true;
        } else { //本機資料筆數比遠端還要少，需要下載
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loginButton.setEnabled(true);
                    uploadBT.setVisibility(View.INVISIBLE);
                }
            });
            mySQLisBig = true;
        }
        return mySQLisBig;
    }
    //endregion

    //請求寫入資料的權限
    private void getPermission() {
        //檢查是否有網路權限
        hasRight = (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE)
                == PackageManager.PERMISSION_GRANTED) && (ActivityCompat.checkSelfPermission(this, Manifest.permission.INTERNET)
                == PackageManager.PERMISSION_GRANTED);
        if (hasRight == false) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_NETWORK_STATE}, 1);
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, 2);
        }
        //請求寫入手機記憶體的權限
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
            //請求權限
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
        } else {
            isPermissionPassed = true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100) {
            if (permissions[0].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    /**如果用戶同意*/
                    isPermissionPassed = true;
                } else {
                    /**如果用戶不同意*/
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this
                            , Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        getPermission();
                    }
                    Toast.makeText(this, "請允許寫入記憶卡權限，否則無法載入相片！", Toast.LENGTH_LONG).show();
                }
            }
        }
    }//onRequestPermissionsResult

    //region 連線遠端Mysql資料庫並下載資料
    //連線到遠端資料庫讀取資料並存入本機資料庫的執行序
    private Runnable mutiThread = new Runnable() {
        @Override
        public void run() {
            conAccount();
            conInspectionItem();
            conInspectionDetectInfo();
            conInspectionDetectResult();
            conInspectionRecord();
            //下載圖片到本機資料夾
            try {
                loadPicFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            synchronized (mutiThread) {
                try {
                    mutiThread.wait(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            //判斷是否有資料表下載失敗
            if (failDownloadCount == 0) {
                //下載資料結束後跳轉頁面
                Intent intent = new Intent(context, CheckSheetActivity.class);
                //禁止返回此Activity
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            } else {
                failDownloadCount = 0; //計數器歸零
                loginButton.setEnabled(true);
                loginButton.setText("請重新下載資料表");
            }
        }
    };

    //連線到Inspection_System資料庫讀取帳號資料表
    public void conAccount() {
        //建立本機資料庫物件
        DataBaseName = "Inspection_System";
        DataBaseTable = "Account";
        DataBaseVersion = 2;
        sqlDataBaseHelper = new SqlDataBaseHelper(this, DataBaseName, null, DataBaseVersion, DataBaseTable);
        db = sqlDataBaseHelper.getWritableDatabase();//開啟資料庫
        switchDB = true;//切換到另一個資料庫
        //建立遠端資料庫實體物件
        MysqlCon con = new MysqlCon(DataBaseName);
        String msg = con.run();
        if (haveConnectDB == false) {
            //顯示連線狀態
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(LoginActivity.this, "Inspection_System資料庫" + msg, Toast.LENGTH_SHORT).show();
                }
            });
            failDownloadCount++;
            return;
        }
        //得到資料的ResultSet，並寫入Sqlite資料庫
        tableName = DataBaseTableName.Account;
        writetosqlite(con.getDataResultSet("SELECT * FROM Account"));

        //region 顯示寫入資料(暫時關閉，測試程式時可使用)
//        String data = con.getData("SELECT * FROM Item");
//        Log.v("OK", data);
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                showData.setText(data);
//            }
//        });
        //endregion

        //pg歸0,必須一直新增Message物件
        Message mssg = new Message();
        mssg.what = 2;
        myHandler.sendMessage(mssg);// 代碼傳給 Handler
        db.close();
    }

    //連線到Inspection_System資料庫讀取檢查項目資料表
    public void conInspectionItem() {
        //建立本機資料庫物件
        DataBaseName = "Inspection_System";
        DataBaseTable = "Item";
        DataBaseVersion = 2;
        sqlDataBaseHelper = new SqlDataBaseHelper(this, DataBaseName, null, DataBaseVersion, DataBaseTable);
        db = sqlDataBaseHelper.getWritableDatabase();//開啟資料庫
        switchDB = true;//切換到另一個資料庫
        //建立遠端資料庫實體物件
        MysqlCon con = new MysqlCon(DataBaseName);
        String msg = con.run();
        if (haveConnectDB == false) {
            //顯示連線狀態
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(LoginActivity.this, "Inspection_System資料庫" + msg, Toast.LENGTH_SHORT).show();
                }
            });
            failDownloadCount++;
            return;
        }
        //得到資料的ResultSet，並寫入Sqlite資料庫
        tableName = DataBaseTableName.Item;
        writetosqlite(con.getDataResultSet("SELECT * FROM Item"));

        //region 顯示寫入資料(暫時關閉，測試程式時可使用)
//        String data = con.getData("SELECT * FROM Item");
//        Log.v("OK", data);
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                showData.setText(data);
//            }
//        });
        //endregion

        //pg歸0,必須一直新增Message物件
        Message mssg = new Message();
        mssg.what = 2;
        myHandler.sendMessage(mssg);// 代碼傳給 Handler
        db.close();
    }

    //連線到Inspection_System資料庫讀取Detect_Info
    public void conInspectionDetectInfo() {
        //建立本機資料庫物件
        DataBaseName = "Inspection_System";
        DataBaseTable = "Detect_Info";
        DataBaseVersion = 2;
        sqlDataBaseHelper = new SqlDataBaseHelper(this, DataBaseName, null, DataBaseVersion, DataBaseTable);
        db = sqlDataBaseHelper.getWritableDatabase();//開啟資料庫
        switchDB = true;//切換到另一個資料庫
        //建立遠端資料庫實體物件
        MysqlCon con = new MysqlCon(DataBaseName);
        String msg = con.run();
        if (haveConnectDB == false) {
            //顯示連線狀態
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(LoginActivity.this, "Inspection_System資料庫" + msg, Toast.LENGTH_SHORT).show();
                }
            });
            failDownloadCount++;
            return;
        }
        //得到資料的ResultSet，並寫入Sqlite資料庫
        tableName = DataBaseTableName.Detect_Info;
        writetosqlite(con.getDataResultSet("SELECT * FROM Detect_Info"));

        //region 顯示寫入資料(暫時關閉，測試程式時可使用)
//        String data = con.getData("SELECT * FROM Item");
//        Log.v("OK", data);
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                showData.setText(data);
//            }
//        });
        //endregion

        //pg歸0,必須一直新增Message物件
        Message mssg = new Message();
        mssg.what = 2;
        myHandler.sendMessage(mssg);// 代碼傳給 Handler
        db.close();
    }

    //連線到Inspection_System資料庫讀取Detect_Result
    public void conInspectionDetectResult() {
        //建立本機資料庫物件
        DataBaseName = "Inspection_System";
        DataBaseTable = "Detect_Result";
        DataBaseVersion = 2;
        sqlDataBaseHelper = new SqlDataBaseHelper(this, DataBaseName, null, DataBaseVersion, DataBaseTable);
        db = sqlDataBaseHelper.getWritableDatabase();//開啟資料庫
        switchDB = true;//切換到另一個資料庫
        //建立遠端資料庫實體物件
        MysqlCon con = new MysqlCon(DataBaseName);
        String msg = con.run();
        if (haveConnectDB == false) {
            //顯示連線狀態
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(LoginActivity.this, "Inspection_System資料庫" + msg, Toast.LENGTH_SHORT).show();
                }
            });
            failDownloadCount++;
            return;
        }
        //得到資料的ResultSet，並寫入Sqlite資料庫
        tableName = DataBaseTableName.Detect_Result;
        writetosqlite(con.getDataResultSet("SELECT * FROM Detect_Result"));

        //region 顯示寫入資料(暫時關閉，測試程式時可使用)
//        String data = con.getData("SELECT * FROM Item");
//        Log.v("OK", data);
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                showData.setText(data);
//            }
//        });
        //endregion

        //pg歸0,必須一直新增Message物件
        Message mssg = new Message();
        mssg.what = 2;
        myHandler.sendMessage(mssg);// 代碼傳給 Handler
        db.close();
    }

    //連線到Inspection_System資料庫讀取Record
    public void conInspectionRecord() {
        //建立本機資料庫物件
        DataBaseName = "Inspection_System";
        DataBaseTable = "Record";
        DataBaseVersion = 2;
        sqlDataBaseHelper = new SqlDataBaseHelper(this, DataBaseName, null, DataBaseVersion, DataBaseTable);
        db = sqlDataBaseHelper.getWritableDatabase();//開啟資料庫
        switchDB = false;//切換到另一個資料庫
        //建立遠端資料庫實體物件
        MysqlCon con = new MysqlCon(DataBaseName);
        String msg = con.run();
        if (haveConnectDB == false) {
            //顯示連線狀態
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(LoginActivity.this, "Inspection_System資料庫" + msg, Toast.LENGTH_SHORT).show();
                }
            });
            failDownloadCount++;
            return;
        }
        //得到資料的ResultSet，並寫入Sqlite資料庫
        tableName = DataBaseTableName.Record;
        writetosqlite(con.getDataResultSet("SELECT * FROM Record"));

        //region 顯示寫入資料(暫時關閉，測試程式時可使用)
//        String data = con.getData("SELECT * FROM Item");
//        Log.v("OK", data);
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                showData.setText(data);
//            }
//        });
        //endregion

        //pg歸0,必須一直新增Message物件
        Message mssg = new Message();
        mssg.what = 3;
        myHandler.sendMessage(mssg);// 代碼傳給 Handler
        db.close();
    }

    //寫入ResultSet資料到Sqlite資料庫
    public void writetosqlite(ResultSet set) {
        ContentValues contentValues = new ContentValues();
        int rowCount = 0;
        switch (tableName) {
            case Account:
                if (set != null) {
                    try {
                        set.last();    // moves cursor to the last row
                        totalRow = set.getRow(); // get row id
                        set.beforeFirst();//鼠標回到第一列之前，不然會拿不到第一列的值
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }
                //先移除所有資料
                db.delete(DataBaseTable, null, null);
                while (true) {
                    try {
                        if (!set.next()) break;
                        else {
                            contentValues.put("Ser", set.getInt("No"));
                            contentValues.put("Account_Type", set.getInt("Account_Type"));
                            contentValues.put("ID", set.getString("ID"));
                            contentValues.put("PWD", set.getString("PWD"));
                            contentValues.put("Enabled", set.getInt("Enabled"));
                            count = db.insert(DataBaseTable, null, contentValues);
                        }
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                    //必須一直新增Message物件
                    Message mssg1 = new Message();
                    mssg1.what = 1;
                    myHandler.sendMessage(mssg1);// 代碼傳給 Handler
                }
                long finalCount = count;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(LoginActivity.this, "已儲存 " + finalCount + " 筆資料至本機資料庫。", Toast.LENGTH_SHORT).show();
                    }
                });
                //count歸零
                count = 0;
                break;
            case Item:
                if (set != null) {
                    try {
                        set.last();    // moves cursor to the last row
                        totalRow = set.getRow(); // get row id
                        set.beforeFirst();//鼠標回到第一列之前，不然會拿不到第一列的值
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }
                //先移除所有資料
                db.delete(DataBaseTable, null, null);
                while (true) {
                    try {
                        if (!set.next()) break;
                        else {
                            contentValues.put("Name", set.getString("Name"));
                            contentValues.put("ClassNo", set.getString("ClassNo"));
                            contentValues.put("Enabled", set.getString("Enabled"));
                            db.insert(DataBaseTable, null, contentValues);
                            rowCount++;
                            count = rowCount;
                        }
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                    //必須一直新增Message物件
                    Message mssg2 = new Message();
                    mssg2.what = 1;
                    myHandler.sendMessage(mssg2);// 代碼傳給 Handler
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                long finalCountItem = count;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(LoginActivity.this, "已儲存 " + finalCountItem + " 筆資料至本機資料庫。", Toast.LENGTH_SHORT).show();
                    }
                });
                //count歸零
                count = 0;
                //tableName = DataBaseTableName.User_Info;
                break;
            case Detect_Info:
                if (set != null) {
                    try {
                        set.last();    // moves cursor to the last row
                        totalRow = set.getRow(); // get row id
                        set.beforeFirst();//鼠標回到第一列之前，不然會拿不到第一列的值
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }
                //先移除所有資料
                db.delete(DataBaseTable, null, null);
                while (true) {
                    try {
                        if (!set.next()) break;
                        else {
                            //contentValues.put("id", set.getString("No"));
                            contentValues.put("DateTime", set.getString("DateTime"));
                            contentValues.put("DI_Ser", set.getString("DI_Ser"));
                            contentValues.put("NameID", set.getString("NameID"));
                            contentValues.put("Is_Checked", set.getString("Is_Checked"));
                            contentValues.put("SheetName", set.getString("SheetName"));
                            contentValues.put("Parts", set.getString("Parts"));
                            db.insert(DataBaseTable, null, contentValues);
                            rowCount++;
                            count = rowCount;
                        }
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                    //必須一直新增Message物件
                    Message mssg2 = new Message();
                    mssg2.what = 1;
                    myHandler.sendMessage(mssg2);// 代碼傳給 Handler
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                finalCountItem = count;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(LoginActivity.this, "已儲存 " + finalCountItem + " 筆資料至本機資料庫。", Toast.LENGTH_SHORT).show();
                    }
                });
                //count歸零
                count = 0;
                //tableName = DataBaseTableName.User_Info;
                break;
            case Detect_Result:
                if (set != null) {
                    try {
                        set.last();    // moves cursor to the last row
                        totalRow = set.getRow(); // get row id
                        set.beforeFirst();//鼠標回到第一列之前，不然會拿不到第一列的值
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }
                //先移除所有資料
                db.delete(DataBaseTable, null, null);
                while (true) {
                    try {
                        if (!set.next()) break;
                        else {
                            contentValues.put("DI_No", set.getString("DI_No"));
                            contentValues.put("Item_No", set.getString("ItemNo"));
                            contentValues.put("ItemName", set.getString("ItemName"));
                            contentValues.put("ISOK", set.getString("ISOK"));
                            contentValues.put("IsRecord", set.getString("IsRecord"));
                            contentValues.put("RecordNo", set.getString("RecordNo"));
                            db.insert(DataBaseTable, null, contentValues);
                            rowCount++;
                            count = rowCount;
                        }
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                    //必須一直新增Message物件
                    Message mssg2 = new Message();
                    mssg2.what = 1;
                    myHandler.sendMessage(mssg2);// 代碼傳給 Handler
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                finalCountItem = count;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(LoginActivity.this, "已儲存 " + finalCountItem + " 筆資料至本機資料庫。", Toast.LENGTH_SHORT).show();
                    }
                });
                //count歸零
                count = 0;
                //tableName = DataBaseTableName.User_Info;
                break;
            case Record:
                if (set != null) {
                    try {
                        set.last();    // moves cursor to the last row
                        totalRow = set.getRow(); // get row id
                        set.beforeFirst();//鼠標回到第一列之前，不然會拿不到第一列的值
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }
                //先移除所有資料
                db.delete(DataBaseTable, null, null);
                while (true) {
                    try {
                        if (!set.next()) break;
                        else {
                            contentValues.put("Picture", set.getString("Picture"));
                            contentValues.put("Comment", set.getString("Comment"));
                            contentValues.put("RecordNo", set.getString("RecordNo"));
                            contentValues.put("PicNo", set.getString("PicNo"));
                            contentValues.put("Parts", set.getString("Parts"));
                            contentValues.put("PartsType", set.getString("PartsType"));
                            contentValues.put("PartsQuantity", set.getString("PartsQuantity"));
                            db.insert(DataBaseTable, null, contentValues);
                            rowCount++;
                            count = rowCount;
                        }
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                    //必須一直新增Message物件
                    Message mssg2 = new Message();
                    mssg2.what = 1;
                    myHandler.sendMessage(mssg2);// 代碼傳給 Handler
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                finalCountItem = count;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(LoginActivity.this, "已儲存 " + finalCountItem + " 筆資料至本機資料庫。", Toast.LENGTH_SHORT).show();
                    }
                });
                //count歸零
                count = 0;
                tableName = DataBaseTableName.Account;
                break;
        }
    }

    //將圖片資料同步到手機
    private void loadPicFile() throws IOException {
        String remoteURL = "smb://192.168.1.30/PCBase/Inspection/";
        SmbFileInputStream inputSmbFile = null;
        FileOutputStream outputFile = null;
        String destDir = NoteActivity.getSDPath();
        int len = 0;
        byte[] buf = new byte[4096];
        try {
            //判斷是否local有Inspection資料夾，沒有則新增
            File localFile = new File(destDir, "Inspection");
            if (!localFile.exists()) {
                localFile.mkdir();
            }
            SmbFile dir = new SmbFile(remoteURL, auth);
            for (SmbFile f : dir.listFiles()) {
                inputSmbFile = new SmbFileInputStream(f);
                outputFile = new FileOutputStream(new File(localFile.getPath() + "/" + f.getName()));
                while ((len = inputSmbFile.read(buf)) > 0) {
                    outputFile.write(buf, 0, len);
                }
            }
            outputFile.flush();
        } catch (IOException e) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(LoginActivity.this, "下載圖片失敗", Toast.LENGTH_SHORT).show();
                }
            });
            e.printStackTrace();
        } finally {
            if (inputSmbFile != null) inputSmbFile.close();
            if (outputFile != null) outputFile.close();
        }
    }
    //endregion

    //region 上傳資料庫按鈕按下的事件處理，處理連線伺服器資料庫
    public void upload(View view) {
        loadingProgressBar.setVisibility(View.VISIBLE);
        //呼叫 mutiThread 執行序
        thread = new Thread(upLoadThread);
        thread.start();
    }

    //連線到遠端資料庫並將本地資料庫寫入
    Runnable upLoadThread = new Runnable() {
        @Override
        public void run() {
            mySharedPreferences.edit().putBoolean("isFinished", false).apply(); //先設置上傳狀態為False
            writeDetectInfo("Detect_Info");
            writeRecord("Record");
            writeDetectResult("Detect_Result");
            //傳送圖片到遠端資料夾
            try {
                uploadPicFile();
            } catch (IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(LoginActivity.this, "上傳圖片失敗", Toast.LENGTH_SHORT).show();
                    }
                });
                e.printStackTrace();
            }
            if (isOK) {
                mySharedPreferences.edit().putBoolean("isFinished", true).apply(); //上傳完成狀態設為true
                Message mssg = new Message();
                mssg.what = 4;
                myHandler.sendMessage(mssg);// 代碼傳給 Handler
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loginButton.setEnabled(true);
                        uploadBT.setVisibility(View.INVISIBLE);
                    }
                });
            }
        }
    };

    private void writeRecord(String record) {
        //建立本機資料庫物件
        DataBaseName = "Inspection_System";
        DataBaseTable = record;
        DataBaseVersion = 2;
        sqlDataBaseHelper = new SqlDataBaseHelper(this, DataBaseName, null, DataBaseVersion, DataBaseTable);
        db = sqlDataBaseHelper.getWritableDatabase();//開啟資料庫
        Cursor c = db.rawQuery("Select * From " + record, null); //得到資料表的所有資料
        c.moveToLast();
        totalRow = c.getCount();
        if (totalRow != 0) {
            c.moveToFirst(); //鼠標移到第一行前面
            writeToMySQL(c, DataBaseName, DataBaseTableName.Record);
        }
    }

    private void writeDetectResult(String detect_result) {
        //建立本機資料庫物件
        DataBaseName = "Inspection_System";
        DataBaseTable = detect_result;
        DataBaseVersion = 2;
        sqlDataBaseHelper = new SqlDataBaseHelper(this, DataBaseName, null, DataBaseVersion, DataBaseTable);
        db = sqlDataBaseHelper.getWritableDatabase();//開啟資料庫
        Cursor c = db.rawQuery("Select * From " + detect_result, null); //得到資料表的所有資料
        c.moveToLast();
        totalRow = c.getCount();
        if (totalRow != 0) {
            c.moveToFirst(); //鼠標移到第一行前面
            writeToMySQL(c, DataBaseName, DataBaseTableName.Detect_Result);
        }
    }

    private void writeDetectInfo(String detect_info) {
        //建立本機資料庫物件
        DataBaseName = "Inspection_System";
        DataBaseTable = detect_info;
        DataBaseVersion = 2;
        sqlDataBaseHelper = new SqlDataBaseHelper(this, DataBaseName, null, DataBaseVersion, DataBaseTable);
        db = sqlDataBaseHelper.getWritableDatabase();//開啟資料庫
        Cursor c = db.rawQuery("Select * From " + detect_info, null); //得到資料表的所有資料
        c.moveToLast();
        totalRow = c.getCount();
        if (totalRow != 0) {
            c.moveToFirst(); //鼠標移到第一行前面
            writeToMySQL(c, DataBaseName, DataBaseTableName.Detect_Info);
        }
    }

    private void writeToMySQL(Cursor c, String db, DataBaseTableName table) {
        //建立遠端資料庫實體物件
        MysqlCon con = new MysqlCon(db);
        String msg = con.run();
        //顯示連線狀態
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(LoginActivity.this, "Inspection_System資料庫" + msg, Toast.LENGTH_SHORT).show();
            }
        });
        isOK = con.writeData(c, table); //寫入遠端MySQL資料庫
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isOK) {
                    Toast.makeText(LoginActivity.this, "成功上傳" + table + "資料表", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(LoginActivity.this, "上傳" + table + "資料表失敗", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //上傳圖片到NAS共享資料夾
    private void uploadPicFile() throws IOException {
        String storePathString = "smb://192.168.1.30/PCBase/Inspection";
        String localPathString = NoteActivity.getSDPath();
        SmbFileOutputStream outputSmbFile = null;
        FileInputStream inputFile = null;
        int len = 0;
        byte[] buf = new byte[4096];
        //定義子資料夾
        SmbFile remoteFile = new SmbFile(storePathString, auth);
        File localPath = new File(localPathString, "Inspection");
        if (!remoteFile.exists()) {
            boolean isCreate = false;
            try {
                remoteFile.mkdir(); //建立資料夾
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //得到localpath下的所有檔案
        File[] fileList = localPath.listFiles();
        //宣告一個CharSequence陣列
        CharSequence[] fileNamelist = new CharSequence[fileList.length];
        for (int i = 0; i < fileNamelist.length; i++) {
            //得到檔案的名稱
            fileNamelist[i] = fileList[i].getName();
            //建立遠端儲存File
            SmbFile smbFile = new SmbFile(storePathString + "/" + fileNamelist[i].toString(), auth);
            outputSmbFile = new SmbFileOutputStream(smbFile);
            inputFile = new FileInputStream(fileList[i]);
            while ((len = inputFile.read(buf)) > 0) {
                outputSmbFile.write(buf, 0, len);
            }
            outputSmbFile.flush();
        }
        if (outputSmbFile != null) outputSmbFile.close();
        if (inputFile != null) inputFile.close();
    }
    //endregion

}
