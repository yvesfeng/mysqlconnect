package com.example.mysqlconnectactivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ManagerSheetAdapter extends RecyclerView.Adapter<ManagerSheetAdapter.ViewHolder> {
    private Context context;
    private List<ManagerSheetBean> managerSheetBeanList;

    public ManagerSheetAdapter(Context context, List<ManagerSheetBean> managerSheetBeanList) {
        this.context = context;
        this.managerSheetBeanList = managerSheetBeanList;
    }

    @NonNull
    @Override
    //當ViewHolder不夠時產生一個新的
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_item_manager_sheet,parent,false);
        return new ViewHolder(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    //重用之前產生的ViewHolder，把特定位置的資料連結上去，準備顯示
    public void onBindViewHolder(ManagerSheetAdapter.ViewHolder holder, int position) {
        //
        final ManagerSheetBean managerSheetBean = managerSheetBeanList.get(position);
        holder.tvSheetName.setText(managerSheetBean.getSheetName());
        holder.tvStatus.setText(managerSheetBean.getStatus());
        holder.tvMember.setText(managerSheetBean.getMember());
        holder.tvDate.setText(managerSheetBean.getTime());
        if (managerSheetBean.getStatus().equals("已逾期")){
            holder.myCard.setBackgroundColor(R.color.secondaryColor);
            holder.tvStatus.setTextColor(Color.RED);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return managerSheetBeanList.size();
    }

    //Adapter 需要一個 ViewHolder，只要實作它的 constructor 就好，保存起來的view會放在itemView裡面
    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvSheetName,tvDate,tvMember,tvStatus;
        CardView myCard;
        ViewHolder(View itemView) {
            super(itemView);
            myCard = itemView.findViewById(R.id.myCardView);
            tvSheetName = itemView.findViewById(R.id.card_description);
            tvDate = itemView.findViewById(R.id.card_plantime);
            tvMember = itemView.findViewById(R.id.card_member);
            tvStatus = itemView.findViewById(R.id.card_status);
        }
    }
}
