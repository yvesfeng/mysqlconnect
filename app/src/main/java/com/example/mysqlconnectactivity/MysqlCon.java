package com.example.mysqlconnectactivity;

import android.database.Cursor;
import android.os.Message;
import android.util.Log;

import com.example.mysqlconnectactivity.ui.login.LoginActivity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MysqlCon {
    // 資料庫定義(連到正式機資料庫)
    String mysql_ip = "60.250.50.196";
    int mysql_port = 10987; // Port 預設為 3306
    String db_name = "WorkRecord";
    public String url = "jdbc:mysql://" + mysql_ip + ":" + mysql_port + "/" + db_name;
    public String db_user = "OutSideUser";
    public String db_password = "7x4r4t7?2jvcua+s+";
    public static boolean haveConnectDB;

    //建構子
    public MysqlCon() {

    }

    public MysqlCon(String DB) {
        this.db_name = DB;
        this.url = "jdbc:mysql://" + mysql_ip + ":" + mysql_port + "/" + db_name;
    }

    public String run() {
        String msg = null;
        try {
            //forName 返回與給定的字串名稱相關聯類或介面的Class物件
            Class.forName("com.mysql.jdbc.Driver");
            Log.v("DB", "加載驅動成功");
            msg = "加載驅動成功";
        } catch (ClassNotFoundException e) {
            Log.e("DB", "加載驅動失敗");
            msg = "加載驅動失敗";
            return msg;
        }

        // 連接資料庫
        try {
            //建立資料庫連線
            Connection con = DriverManager.getConnection(url, db_user, db_password);
            Log.v("DB", "遠端連接成功");
            haveConnectDB = true;
            msg = "遠端連接成功";
        } catch (SQLException e) {
            Log.e("DB", "遠端連接失敗");
            Log.e("DB", e.toString());
            haveConnectDB = false;
            msg = "遠端連接失敗";
        }
        return msg;
    }

    public String getData(String sqlString) {
        String data = "";
        try {
            Connection con = DriverManager.getConnection(url, db_user, db_password);
            String sql = sqlString;
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                String name = rs.getString("Name");
                String classno = rs.getString("ClassNo");
                String enable = rs.getString("Enabled");
                data += name + ", " + classno + ", " + enable + "\n";
            }
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    //寫入MySQL資料表
    public boolean writeData(Cursor c, LoginActivity.DataBaseTableName table) {
        boolean isFinished = false;
        Connection con = null;
        try {
            con = DriverManager.getConnection(url, db_user, db_password);
            Statement st = con.createStatement();
            switch (table) {
                case Detect_Info:
                    st.execute("DELETE FROM " + table);
                    do {
                        String NO = c.getString(1) + "-" + c.getString(2);
                        st.executeUpdate("INSERT INTO " + table + " (No,DateTime,DI_Ser,NameID,Is_Checked,SheetName,Parts) " + "VALUES ('" + NO + "','" + c.getString(1) + "'," + c.getString(2) + ",'" + c.getString(3) + "'," + c.getString(4) + ",'" + c.getString(5) + "','" + c.getString(6) + "')");
                        LoginActivity.count++;
                        //必須一直新增Message物件
                        Message mssg1 = new Message();
                        mssg1.what = 1;
                        LoginActivity.myHandler.sendMessage(mssg1);// 代碼傳給 Handler
                    } while (c.moveToNext());
                    Message mssg1 = new Message();
                    mssg1.what = 2;
                    LoginActivity.myHandler.sendMessage(mssg1);// 代碼傳給 Handler
                    //count歸零
                    LoginActivity.count = 0;
                    break;
                case Detect_Result:
                    st.execute("DELETE FROM " + table);
                    do {
                        st.executeUpdate("INSERT INTO " + table + " (DI_No,ItemNo,ItemName,ISOK,IsRecord,RecordNo) " + "VALUES ('" + c.getString(1) + "'," + c.getString(2) + ",'" + c.getString(3) + "'," + c.getString(4) + "," + c.getString(5) + ",'" + c.getString(6) + "')");
                        LoginActivity.count++;
                        //必須一直新增Message物件
                        mssg1 = new Message();
                        mssg1.what = 1;
                        LoginActivity.myHandler.sendMessage(mssg1);// 代碼傳給 Handler
                    } while (c.moveToNext());
                    mssg1 = new Message();
                    mssg1.what = 2;
                    LoginActivity.myHandler.sendMessage(mssg1);// 代碼傳給 Handler
                    //count歸零
                    LoginActivity.count = 0;
                    break;
                case Record:
                    st.execute("DELETE FROM " + table);
                    do {
                        st.executeUpdate("INSERT INTO " + table + " (RecordNo,Picture,Comment,PicNo,Parts,PartsType,PartsQuantity) " + "VALUES ('" + c.getString(3) + "','" + c.getString(1) + "','" + c.getString(2) + "'," + c.getInt(4) + ",'" + c.getString(5) + "','" + c.getString(6) + "'," + c.getInt(7) + ")");
                        LoginActivity.count++;
                        //必須一直新增Message物件
                        mssg1 = new Message();
                        mssg1.what = 1;
                        LoginActivity.myHandler.sendMessage(mssg1);// 代碼傳給 Handler
                    } while (c.moveToNext());
                    mssg1 = new Message();
                    mssg1.what = 2;
                    LoginActivity.myHandler.sendMessage(mssg1);// 代碼傳給 Handler
                    //count歸零
                    LoginActivity.count = 0;
                    break;
            }
            st.close();
            isFinished = true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return isFinished;
        }
        return isFinished;
    }

    public ResultSet getDataResultSet() {
        ResultSet rs = null;
        try {
            Connection con = DriverManager.getConnection(url, db_user, db_password);
            String sql = "SELECT * FROM User_Info";
            Statement st = con.createStatement();
            rs = st.executeQuery(sql);
            //st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }

    public ResultSet getDataResultSet(String sqlString) {
        ResultSet rs = null;
        try {
            Connection con = DriverManager.getConnection(url, db_user, db_password);
            String sql = sqlString;
            Statement st = con.createStatement();
            rs = st.executeQuery(sql);
            //st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }

}
